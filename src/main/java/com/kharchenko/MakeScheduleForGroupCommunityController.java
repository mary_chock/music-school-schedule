package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class MakeScheduleForGroupCommunityController {

    @FXML
    public Button createSchedCommExitBtn;
    @FXML
    public Button createSchedCommSaveBtn;
    @FXML
    public DatePicker startDate;
    @FXML
    public DatePicker endDate;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox;
    @FXML
    public ChoiceBox<String> weekDayBox;
    @FXML
    public ChoiceBox<LessonEntity> lessonBox;
    @FXML
    public ChoiceBox<GroupEntity> groupBox;
    @FXML
    public ChoiceBox<CommunityEntity> communityBox;
    @FXML
    public ChoiceBox<StudentEntity> studentBox;
    @FXML
    public Label studentLabel;
    @FXML
    public Label groupLabel;

    private final Resource fxml1;
    private final ApplicationContext applicationContext;
    private final LessonDao lessonDao;
    private final GroupDao groupDao;
    private final CommunityDao communityDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private final TeacherDao teacherDao;
    private final ScheduleDao scheduleDao;
    private final TeacherStudentDao teacherStudentDao;
    private final StudentSubjectDao studentSubjectDao;
    private WorkTypeEntity workTypeEntity;

    public MakeScheduleForGroupCommunityController(@Value("classpath:/scenes/teacher-schedule.fxml") Resource fxml1,
                                                   ApplicationContext applicationContext,
                                                   LessonDao lessonDao, GroupDao groupDao,
                                                   CommunityDao communityDao,
                                                   SubjectTeacherDao subjectTeacherDao,
                                                   TeacherDao teacherDao,
                                                   TeacherStudentDao teacherStudentDao,
                                                   StudentSubjectDao studentSubjectDao,
                                                   ScheduleDao scheduleDao) {
        this.fxml1 = fxml1;
        this.applicationContext = applicationContext;
        this.lessonDao = lessonDao;
        this.groupDao = groupDao;
        this.communityDao = communityDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.teacherDao = teacherDao;
        this.teacherStudentDao = teacherStudentDao;
        this.studentSubjectDao = studentSubjectDao;
        this.scheduleDao = scheduleDao;
    }

    @FXML
    public void initialize() {
        setInitialVisibility();
        setSubjectItems();
        setWeekDayItems();
        setLessonItems();
        ChangeListener<SubjectEntity> changeListener = (observableValue, studentEntity, s1) -> {
            if (s1 != null) {
                setGroupOrCommunity();
            }
        };
        subjectBox.getSelectionModel().selectedItemProperty().addListener(changeListener);
        ChangeListener<GroupEntity> changeListener1 = (observableValue, studentEntity, s1) -> {
            if (s1 != null) {
                if (workTypeEntity.getName().equals("Индивидуальная")) {
                    setStudentItems();
                }
            }
        };
        groupBox.getSelectionModel().selectedItemProperty().addListener(changeListener1);
        createSchedCommExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(fxml1, createSchedCommExitBtn);
        });
        createSchedCommSaveBtn.setOnAction(actionEvent -> saveSchedule());
    }

    private boolean checkDates() {
        if (startDate.getValue() != null && endDate.getValue() != null) {
            return endDate.getValue().isAfter(startDate.getValue());
        }
        return false;
    }

    private boolean checkBoxes() {
        if (subjectBox.getValue() != null && checkDates() && weekDayBox.getValue() != null && lessonBox
                .getValue() != null) {
            switch (workTypeEntity.getName()) {
                case "Индивидуальная":
                    if (groupBox.getValue() != null && studentBox.getValue() != null) {
                        return true;
                    }
                    break;
                case "Групповая":
                    if (groupBox.getValue() != null)
                        return true;
                    break;
                case "Коллективная":
                    if (communityBox.getValue() != null)
                        return true;
                    break;
            }
        }
        return false;
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение записи");
        alert.setHeaderText("Добавить запись в расписание?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void saveSchedule() {
        if (checkBoxes())
            if (showSaveConfirmation()) {
                ScheduleEntity scheduleEntity = new ScheduleEntity();
                scheduleEntity.setStartDate(java.sql.Date.valueOf(startDate.getValue()));
                scheduleEntity.setEndDate(java.sql.Date.valueOf(endDate.getValue()));
                scheduleEntity.setScheduleLessonEntity(lessonBox.getValue());
                scheduleEntity.setWeekDay(weekDayBox.getValue());
                scheduleEntity.setScheduleTeacherEntity(AuthorizationController.getLoggedTeacher());

                if (groupBox.getValue() != null)
                    scheduleEntity.setScheduleGroupEntity(groupBox.getValue());
                else
                    scheduleEntity.setScheduleGroupEntity(null);

                if (communityBox.getValue() != null)
                    scheduleEntity.setScheduleCommunityEntity(communityBox.getValue());
                else
                    scheduleEntity.setScheduleCommunityEntity(null);

                for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                    if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId()
                                            .equals(AuthorizationController.getLoggedTeacher()
                                                                           .getId()) && subjectTeacherEntity
                            .getSubjectTeacherSubjectEntity().getId().equals(subjectBox.getValue().getId()))
                        scheduleEntity.setScheduleTeacherSubjectEntity(subjectTeacherEntity);
                }

                if (studentBox.getValue() != null) {
                    for (TeacherStudentEntity teacherStudentEntity : teacherStudentDao.getAll()) {
                        if (teacherStudentEntity.getTeacherTeacherStudentEntity().getId()
                                                .equals(AuthorizationController.getLoggedTeacher()
                                                                               .getId()) && teacherStudentEntity
                                .getStudentTeacherStudentEntity().getId().equals(studentBox.getValue().getId()))
                            scheduleEntity.setScheduleTeacherStudentEntity(teacherStudentEntity);
                    }
                    for (StudentSubjectEntity studentSubjectEntity : studentSubjectDao.getAll()) {
                        if (studentSubjectEntity.getSubjectStudentSubjectEntity().getId()
                                                .equals(subjectBox.getValue().getId()) && studentSubjectEntity
                                .getStudentStudentSubjectEntity().getId().equals(studentBox.getValue().getId()))
                            scheduleEntity.setScheduleStudentSubjectEntity(studentSubjectEntity);
                    }
                } else {
                    scheduleEntity.setScheduleTeacherStudentEntity(null);
                    scheduleEntity.setScheduleStudentSubjectEntity(null);
                }

                scheduleDao.create(scheduleEntity);

                changeScene(fxml1, createSchedCommExitBtn);
            }
    }

    private void setStudentItems() {
        ObservableList<StudentEntity> students = FXCollections.observableArrayList();
        List<TeacherStudentEntity> all = teacherStudentDao.getAll();
        for (StudentSubjectEntity studentSubjectEntity : studentSubjectDao.getAll()) {
            if (studentSubjectEntity.getStudentStudentSubjectEntity().getStudentEducationYearEntity().getId()
                                    .equals(groupBox.getValue().getGroupEducationYearEntity().getId())) {
                if (studentSubjectEntity.getSubjectStudentSubjectEntity().getId()
                                        .equals(subjectBox.getValue().getId())) {
                    for (TeacherStudentEntity teacherStudentEntity : all) {
                        if (studentSubjectEntity.getStudentStudentSubjectEntity().getId()
                                                .equals(teacherStudentEntity.getStudentTeacherStudentEntity()
                                                                            .getId()) && teacherStudentEntity
                                .getTeacherTeacherStudentEntity().getId()
                                .equals(AuthorizationController.getLoggedTeacher().getId())) {
                            students.add(studentSubjectEntity.getStudentStudentSubjectEntity());
                            break;
                        }
                    }
                }
            }
        }
        studentBox.setItems(students);
    }

    private void setGroupOrCommunity() {
        workTypeEntity = subjectBox.getValue().getWorkTypeEntity();
        switch (workTypeEntity.getName()) {
            case "Индивидуальная":
                groupBox.setVisible(true);
                communityBox.setVisible(false);
                studentBox.setVisible(true);
                studentLabel.setVisible(true);
                groupLabel.setVisible(true);
                groupLabel.setText("Класс");
                setGroupItems();
                break;
            case "Групповая":
                groupBox.setVisible(true);
                communityBox.setVisible(false);
                studentBox.setVisible(false);
                studentLabel.setVisible(false);
                groupLabel.setVisible(true);
                groupLabel.setText("Класс");
                setGroupItems();
                break;
            case "Коллективная":
                groupBox.setVisible(false);
                communityBox.setVisible(true);
                studentBox.setVisible(false);
                studentLabel.setVisible(false);
                groupLabel.setVisible(true);
                groupLabel.setText("Коллектив");
                setCommunityItems();
                break;
        }
    }

    private void setCommunityItems() {
        ObservableList<CommunityEntity> communities = FXCollections.observableArrayList();
        for (CommunityEntity communityEntity : communityDao.getAll()) {
            if (communityEntity.getTeacherEntity().getId().equals(AuthorizationController.getLoggedTeacher().getId()))
                communities.add(communityEntity);
        }
        communityBox.setItems(communities);
    }

    private void setGroupItems() {
        ObservableList<GroupEntity> groups = FXCollections.observableArrayList();
        groups.addAll(groupDao.getAll());
        groupBox.setItems(groups);
    }

    private void setLessonItems() {
        ObservableList<LessonEntity> lessons = FXCollections.observableArrayList();
        lessons.addAll(lessonDao.getAll());
        lessonBox.setItems(lessons);
    }

    private void setSubjectItems() {
        int count = 0;
        ObservableList<SubjectEntity> subjects = FXCollections.observableArrayList();
        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
            if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId()
                                    .equals(AuthorizationController.getLoggedTeacher().getId())) {
                if (subjectTeacherEntity.getSubjectTeacherSubjectEntity().getName().equals("Коллектив")) {
                    count++;
                }
                if (count <= 1)
                    subjects.add(subjectTeacherEntity.getSubjectTeacherSubjectEntity());
            }
        }
        subjectBox.setItems(subjects);
    }

    private void setWeekDayItems() {
        ObservableList<String> weekDays = FXCollections.observableArrayList();
        List<String> weekDay = new ArrayList<>();
        weekDay.add("Понедельник");
        weekDay.add("Вторник");
        weekDay.add("Среда");
        weekDay.add("Четверг");
        weekDay.add("Пятница");
        weekDay.add("Суббота");
        weekDays.addAll(weekDay);
        weekDayBox.setItems(weekDays);
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void setInitialVisibility() {
        studentLabel.setVisible(false);
        groupLabel.setVisible(false);
        groupBox.setVisible(false);
        communityBox.setVisible(false);
        studentBox.setVisible(false);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
