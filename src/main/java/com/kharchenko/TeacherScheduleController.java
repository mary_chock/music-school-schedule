package com.kharchenko;

import com.kharchenko.entities.LessonEntity;
import com.kharchenko.entities.ScheduleEntity;
import com.kharchenko.services.dao.LessonDao;
import com.kharchenko.services.dao.ScheduleDao;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TeacherScheduleController {

    @FXML
    public Button scheduleCreateBtn;
    @FXML
    public Button scheduleDeleteBtn;
    @FXML
    public Button scheduleExitBtn;
    @FXML
    public TableView<ScheduleEntity> table;
    @FXML
    public ListView<String> list;
    @FXML
    public ListView<String> list1;
    @FXML
    public ListView<String> list2;
    @FXML
    public ListView<String> list3;
    @FXML
    public ListView<String> list4;
    @FXML
    public ListView<String> list5;
    @FXML
    public ListView<String> list6;
    @FXML
    public Label teacherNameLabel;
    @FXML
    public Label week1Label;
    @FXML
    public Label week2Label;
    @FXML
    public Label week3Label;
    @FXML
    public Label week4Label;
    @FXML
    public Label week5Label;
    @FXML
    public Label week6Label;

    private final Resource fxml1;
    private final Resource fxml2;
    private final ApplicationContext applicationContext;
    private final ScheduleDao scheduleDao;
    private final LessonDao lessonDao;

    public TeacherScheduleController(@Value("classpath:/scenes/make-schedule-for-group-community.fxml") Resource fxml1,
                                     @Value("classpath:/scenes/delete-schedule.fxml") Resource fxml2,
                                     ApplicationContext applicationContext,
                                     ScheduleDao scheduleDao,
                                     LessonDao lessonDao) {

        this.fxml1 = fxml1;
        this.fxml2 = fxml2;
        this.applicationContext = applicationContext;
        this.scheduleDao = scheduleDao;
        this.lessonDao = lessonDao;
    }

    @FXML
    public void initialize() {
        teacherNameLabel.setText(AuthorizationController.getLoggedTeacher().toString());
        setScheduleItems();
        scheduleExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                Platform.exit();
        });
        scheduleCreateBtn.setOnAction(actionEvent -> changeScene(fxml1, scheduleCreateBtn));
        scheduleDeleteBtn.setOnAction(actionEvent -> changeScene(fxml2, scheduleDeleteBtn));
    }

    private void setScheduleItems() {
        List<ScheduleEntity> schedules = new ArrayList<>();
        for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
            if (scheduleEntity.getScheduleTeacherEntity().getId()
                              .equals(AuthorizationController.getLoggedTeacher().getId())) {
                schedules.add(scheduleEntity);
            }
        }
        ObservableList<String> lessons = FXCollections.observableArrayList();
        for (LessonEntity lessonEntity : lessonDao.getAll()) {
            lessons.add(lessonEntity.getLessonNumber() + ".\n" + lessonEntity.getStartTime() + "\n" + lessonEntity
                    .getEndTime()+"\n ");
        }
        ObservableList<String> week1 = FXCollections.observableArrayList();
        ObservableList<String> week2 = FXCollections.observableArrayList();
        ObservableList<String> week3 = FXCollections.observableArrayList();
        ObservableList<String> week4 = FXCollections.observableArrayList();
        ObservableList<String> week5 = FXCollections.observableArrayList();
        ObservableList<String> week6 = FXCollections.observableArrayList();
        String[] w1 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w2 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w3 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w4 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w5 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w6 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        for (ScheduleEntity schedule : schedules) {
            if (schedule.getWeekDay().equals(String.valueOf(week1Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w1[index]=schedule.getInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week2Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w2[index]=schedule.getInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week3Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w3[index]=schedule.getInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week4Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w4[index]=schedule.getInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week5Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w5[index]=schedule.getInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week6Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w6[index]=schedule.getInfo();
            }
        }
        week1.addAll(w1);
        week2.addAll(w2);
        week3.addAll(w3);
        week4.addAll(w4);
        week5.addAll(w5);
        week6.addAll(w6);
        list.setItems(lessons);
        list1.setItems(week1);
        list2.setItems(week2);
        list3.setItems(week3);
        list4.setItems(week4);
        list5.setItems(week5);
        list6.setItems(week6);
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Закрыть приложение?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
