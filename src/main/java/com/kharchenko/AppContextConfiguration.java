package com.kharchenko;

import com.kharchenko.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;
import static org.hibernate.cfg.AvailableSettings.SHOW_SQL;

@Configuration
@PropertySource("classpath:/application.properties")
@ComponentScan("com.kharchenko")
public class AppContextConfiguration {

    @Bean
    public SessionFactory sessionFactory() {
        try {
            org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
            Properties settings = new Properties();
            settings.put(DRIVER, "org.postgresql.Driver");
            settings.put(URL, "jdbc:postgresql://localhost:5432/music-school-schedule?useSSL=false");
            settings.put(USER, "postgres");
            settings.put(PASS, "2132");
            settings.put(DIALECT, "org.hibernate.dialect.MySQL5Dialect");
            settings.put(SHOW_SQL, "true");
            settings.setProperty("hibernate.current_session_context_class", "thread");
            configuration.setProperties(settings);
            configuration.addAnnotatedClass(CommunityCompositionEntity.class);
            configuration.addAnnotatedClass(CommunityEntity.class);
            configuration.addAnnotatedClass(EducationYearEntity.class);
            configuration.addAnnotatedClass(GroupCompositionEntity.class);
            configuration.addAnnotatedClass(GroupEntity.class);
            configuration.addAnnotatedClass(LessonEntity.class);
            configuration.addAnnotatedClass(OfficeEntity.class);
            configuration.addAnnotatedClass(ScheduleEntity.class);
            configuration.addAnnotatedClass(StudentEntity.class);
            configuration.addAnnotatedClass(SubjectEntity.class);
            configuration.addAnnotatedClass(SubjectTeacherEntity.class);
            configuration.addAnnotatedClass(TeacherEntity.class);
            configuration.addAnnotatedClass(TeacherStudentEntity.class);
            configuration.addAnnotatedClass(WorkTypeEntity.class);
            configuration.addAnnotatedClass(StudentSubjectEntity.class);
            ServiceRegistry serviceRegistry =
                    new StandardServiceRegistryBuilder().applySettings(configuration.getProperties())
                                                        .build();
            return configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
