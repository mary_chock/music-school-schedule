package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class AddStudentController {

    @FXML
    public Button newStudentExitBtn;
    @FXML
    public Button newStudentSaveBtn;
    @FXML
    public Button newStudentAddSubjectBtn;
    @FXML
    public Button newStudentDeleteSubjectBtn1;
    @FXML
    public Button newStudentDeleteSubjectBtn2;
    @FXML
    public Button newStudentDeleteSubjectBtn3;
    @FXML
    public Button newStudentDeleteSubjectBtn4;
    @FXML
    public Button newStudentDeleteSubjectBtn5;
    @FXML
    public Button newStudentDeleteSubjectBtn6;
    @FXML
    public Button newStudentDeleteSubjectBtn7;
    @FXML
    public TextField firstName;
    @FXML
    public TextField secondName;
    @FXML
    public TextField lastName;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox1;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox2;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox3;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox4;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox5;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox6;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox7;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox1;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox2;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox3;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox4;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox5;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox6;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox7;
    @FXML
    public ChoiceBox<EducationYearEntity> classNumberBox;

    private final Resource fxml4;
    private final ApplicationContext applicationContext;
    private final StudentDao studentDao;
    private final TeacherDao teacherDao;
    private final EducationYearDao educationYearDao;
    private final SubjectDao subjectDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private final GroupCompositionDao groupCompositionDao;
    private final TeacherStudentDao teacherStudentDao;
    private final GroupDao groupDao;
    private final StudentSubjectDao studentSubjectDao;
    private int count;
    private List<ChoiceBox<SubjectEntity>> subjects;
    private List<ChoiceBox<TeacherEntity>> teachers;
    private List<SubjectTeacherEntity> subjectTeacherEntities;

    public AddStudentController(@Value("classpath:/scenes/main-menu-db.fxml") Resource fxml4,
                                ApplicationContext applicationContext,
                                TeacherDao teacherDao,
                                SubjectDao subjectDao,
                                StudentDao studentDao,
                                EducationYearDao educationYearDao,
                                SubjectTeacherDao subjectTeacherDao,
                                GroupCompositionDao groupCompositionDao,
                                GroupDao groupDao,
                                TeacherStudentDao teacherStudentDao,
                                StudentSubjectDao studentSubjectDao) {
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
        this.teacherDao = teacherDao;
        this.subjectDao = subjectDao;
        this.studentDao = studentDao;
        this.educationYearDao = educationYearDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.groupCompositionDao = groupCompositionDao;
        this.groupDao = groupDao;
        this.teacherStudentDao = teacherStudentDao;
        this.studentSubjectDao = studentSubjectDao;
    }

    @FXML
    public void initialize() {
        count = 1;
        subjectTeacherEntities = subjectTeacherDao.getAll();
        setInitialVisibility();
        initBoxesLists();
        setSubjectItems(subjectBox);
        setClassItems(classNumberBox);
        this.newStudentExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(this.fxml4, newStudentExitBtn);
        });
        this.newStudentAddSubjectBtn.setOnAction(actionEvent -> addSubject());
        this.teacherBox.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox, subjectBox));
        this.teacherBox1.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox1, subjectBox1));
        this.teacherBox2.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox2, subjectBox2));
        this.teacherBox3.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox3, subjectBox3));
        this.teacherBox4.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox4, subjectBox4));
        this.teacherBox5.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox5, subjectBox5));
        this.teacherBox6.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox6, subjectBox6));
        this.teacherBox7.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(teacherBox7, subjectBox7));
        this.newStudentDeleteSubjectBtn1.setOnAction(actionEvent -> clear(subjectBox1, teacherBox1));
        this.newStudentDeleteSubjectBtn2.setOnAction(actionEvent -> clear(subjectBox2, teacherBox2));
        this.newStudentDeleteSubjectBtn3.setOnAction(actionEvent -> clear(subjectBox3, teacherBox3));
        this.newStudentDeleteSubjectBtn4.setOnAction(actionEvent -> clear(subjectBox4, teacherBox4));
        this.newStudentDeleteSubjectBtn5.setOnAction(actionEvent -> clear(subjectBox5, teacherBox5));
        this.newStudentDeleteSubjectBtn6.setOnAction(actionEvent -> clear(subjectBox6, teacherBox6));
        this.newStudentDeleteSubjectBtn7.setOnAction(actionEvent -> clear(subjectBox7, teacherBox7));
        this.newStudentSaveBtn.setOnAction(actionEvent -> saveTeacher());
    }

    private boolean checkInput() {
        String nameRegEx = "^[А-Я].+[а-я]$";
        if (!firstName.getText().isEmpty()
                && !secondName.getText().isEmpty()
                && !lastName.getText().isEmpty()) {

            if (!firstName.getText().matches(nameRegEx)) {
                setAlert("Имя должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!secondName.getText().matches(nameRegEx)) {
                setAlert("Отчество должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!lastName.getText().matches(nameRegEx)) {
                setAlert("Фамилия должна начинаться с заглавной буквы, только русский язык");
                return false;
            }
        }
        return true;
    }

    private void setAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Неправильный ввод");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private boolean checkSubject(ChoiceBox<SubjectEntity> subject, ChoiceBox<TeacherEntity> teacher) {
        return subject.getValue() != null && teacher.getValue() != null;
    }

    private GroupEntity getGroupFromClass() {
        for (GroupEntity groupEntity : groupDao.getAll()) {
            if (groupEntity.getGroupEducationYearEntity().getId().equals(classNumberBox.getValue().getId()))
                return groupEntity;
        }
        return null;
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение ученика");
        alert.setHeaderText("Сохранить нового ученика?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void saveTeacher() {
        if (checkInput() && classNumberBox.getValue() != null)
            if (showSaveConfirmation()) {
                if (checkSubject(subjectBox, teacherBox)) {
                    if (!hasSame()) {
                        StudentEntity studentEntity = new StudentEntity();
                        studentEntity.setFirstName(firstName.getText());
                        studentEntity.setSecondName(secondName.getText());
                        studentEntity.setLastName(lastName.getText());
                        studentEntity.setStudentEducationYearEntity(classNumberBox.getValue());

                        GroupCompositionEntity groupCompositionEntity = new GroupCompositionEntity();
                        groupCompositionEntity.setStudentGroupCompositionEntity(studentEntity);
                        groupCompositionEntity.setGroupGroupCompositionEntity(getGroupFromClass());

                        TeacherStudentEntity teacherStudentEntity1 = new TeacherStudentEntity();
                        teacherStudentEntity1.setStudentTeacherStudentEntity(studentEntity);
                        teacherStudentEntity1.setTeacherTeacherStudentEntity(teacherBox.getValue());

                        SubjectTeacherEntity subjectTeacherEntity1 = null;
                        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                            if (subjectTeacherEntity.getSubjectTeacherSubjectEntity().getId()
                                                    .equals(subjectBox.getValue().getId()) && subjectTeacherEntity
                                    .getTeacherTeacherSubjectEntity().getId()
                                    .equals(teacherBox.getValue().getId()))
                                subjectTeacherEntity1 = subjectTeacherEntity;
                        }

                        StudentSubjectEntity studentSubjectEntity = new StudentSubjectEntity();
                        studentSubjectEntity.setStudentStudentSubjectEntity(studentEntity);
                        studentSubjectEntity.setSubjectStudentSubjectEntity(subjectBox.getValue());
                        studentSubjectEntity.setTeacherSubjectStudentSubjectEntity(subjectTeacherEntity1);

                        studentDao.create(studentEntity);
                        groupCompositionDao.create(groupCompositionEntity);
                        teacherStudentDao.create(teacherStudentEntity1);
                        studentSubjectDao.create(studentSubjectEntity);

                        for (int i = 0; i < 7; i++) {
                            if (checkSubject(subjects.get(i), teachers.get(i))) {
                                TeacherStudentEntity teacherStudentEntity = new TeacherStudentEntity();
                                teacherStudentEntity.setStudentTeacherStudentEntity(studentEntity);
                                teacherStudentEntity.setTeacherTeacherStudentEntity(teachers.get(i).getValue());
                                teacherStudentDao.create(teacherStudentEntity);

                                SubjectTeacherEntity subjectTeacherEntity2 = null;
                                for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                                    if (subjectTeacherEntity.getSubjectTeacherSubjectEntity().getId()
                                                            .equals(subjects.get(i).getValue()
                                                                            .getId()) && subjectTeacherEntity
                                            .getTeacherTeacherSubjectEntity().getId()
                                            .equals(teachers.get(i).getValue().getId()))
                                        subjectTeacherEntity2 = subjectTeacherEntity;
                                }

                                StudentSubjectEntity studentSubjectEntity1 = new StudentSubjectEntity();
                                studentSubjectEntity1.setStudentStudentSubjectEntity(studentEntity);
                                studentSubjectEntity1.setSubjectStudentSubjectEntity(subjectBox.getValue());
                                studentSubjectEntity1.setTeacherSubjectStudentSubjectEntity(subjectTeacherEntity2);
                            }
                        }

                        changeScene(fxml4, newStudentAddSubjectBtn);
                    }
                }
            }
    }

    private boolean hasSame() {
        Set<String> strings = new TreeSet<>();
        strings.add(subjectBox.getValue().getName());
        int counter = 1;
        for (ChoiceBox<SubjectEntity> subject : subjects) {
            if (subject.getValue() != null) {
                strings.add(subject.getValue().getName());
                counter++;
            }
        }
        if (strings.size() < counter) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Вы выбрали одинаковые дисциплины");
            alert.showAndWait();
            return true;
        }

        return false;
    }

    private void clear(ChoiceBox<SubjectEntity> subject, ChoiceBox<TeacherEntity> teacher) {
        subject.setValue(null);
        teacher.setValue(null);
        subject.setVisible(false);
        teacher.setVisible(false);
        subject.setVisible(true);
        teacher.setVisible(true);
    }

    private void setVisible(ChoiceBox<SubjectEntity> subject, ChoiceBox<TeacherEntity> teacher, Button delete) {
        subject.setVisible(true);
        teacher.setVisible(true);
        delete.setVisible(true);
        count++;
    }

    private void addSubject() {
        switch (count) {
            case 1:
                if (subjectBox.getValue() != null && teacherBox.getValue() != null) {
                    setVisible(subjectBox1, teacherBox1, newStudentDeleteSubjectBtn1);
                    setSubjectItems(subjectBox1);
                }
                break;
            case 2:
                if (subjectBox1.getValue() != null && teacherBox1.getValue() != null) {
                    setVisible(subjectBox2, teacherBox2, newStudentDeleteSubjectBtn2);
                    setSubjectItems(subjectBox2);
                }
                break;
            case 3:
                if (subjectBox2.getValue() != null && teacherBox2.getValue() != null) {
                    setVisible(subjectBox3, teacherBox3, newStudentDeleteSubjectBtn3);
                    setSubjectItems(subjectBox3);
                }
                break;
            case 4:
                if (subjectBox3.getValue() != null && teacherBox3.getValue() != null) {
                    setVisible(subjectBox4, teacherBox4, newStudentDeleteSubjectBtn4);
                    setSubjectItems(subjectBox4);
                }
                break;
            case 5:
                if (subjectBox4.getValue() != null && teacherBox4.getValue() != null) {
                    setVisible(subjectBox5, teacherBox5, newStudentDeleteSubjectBtn5);
                    setSubjectItems(subjectBox5);
                }
                break;
            case 6:
                if (subjectBox5.getValue() != null && teacherBox5.getValue() != null) {
                    setVisible(subjectBox6, teacherBox6, newStudentDeleteSubjectBtn6);
                    setSubjectItems(subjectBox6);
                }
                break;
            case 7:
                if (subjectBox6.getValue() != null && teacherBox6.getValue() != null) {
                    setVisible(subjectBox7, teacherBox7, newStudentDeleteSubjectBtn7);
                    setSubjectItems(subjectBox7);
                }
                break;
        }
    }

    private void checkSubjectForTeacher(ChoiceBox<TeacherEntity> teacher, ChoiceBox<SubjectEntity> subject) {
        if (subject.getValue() != null)
            setTeacherItems(teacher, subject);
    }

    private void setTeacherItems(ChoiceBox<TeacherEntity> teacher, ChoiceBox<SubjectEntity> subject) {

        ObservableList<TeacherEntity> teachers = FXCollections.observableArrayList();
        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherEntities) {
            if (subjectTeacherEntity.getSubjectTeacherSubjectEntity().getId().equals(subject.getValue().getId()))
                teachers.add(subjectTeacherEntity.getTeacherTeacherSubjectEntity());
        }
        teacher.setItems(teachers);
    }

    private void setClassItems(ChoiceBox<EducationYearEntity> year) {
        ObservableList<EducationYearEntity> years = FXCollections.observableArrayList();
        years.addAll(educationYearDao.getAll());
        year.setItems(years);
    }

    private void setSubjectItems(ChoiceBox<SubjectEntity> subject) {
        ObservableList<SubjectEntity> subjects = FXCollections.observableArrayList();
        for (SubjectEntity subjectEntity : subjectDao.getAll()) {
            if (!subjectEntity.getName().equals("Коллектив"))
                subjects.add(subjectEntity);
        }
        subject.setItems(subjects);
    }

    private void initBoxesLists() {
        teachers = new ArrayList<>();
        subjects = new ArrayList<>();
        teachers.add(teacherBox1);
        teachers.add(teacherBox2);
        teachers.add(teacherBox3);
        teachers.add(teacherBox4);
        teachers.add(teacherBox5);
        teachers.add(teacherBox6);
        teachers.add(teacherBox7);
        subjects.add(subjectBox1);
        subjects.add(subjectBox2);
        subjects.add(subjectBox3);
        subjects.add(subjectBox4);
        subjects.add(subjectBox5);
        subjects.add(subjectBox6);
        subjects.add(subjectBox7);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setInitialVisibility() {
        this.newStudentDeleteSubjectBtn1.setVisible(false);
        this.newStudentDeleteSubjectBtn2.setVisible(false);
        this.newStudentDeleteSubjectBtn3.setVisible(false);
        this.newStudentDeleteSubjectBtn4.setVisible(false);
        this.newStudentDeleteSubjectBtn5.setVisible(false);
        this.newStudentDeleteSubjectBtn6.setVisible(false);
        this.newStudentDeleteSubjectBtn7.setVisible(false);
        this.teacherBox1.setVisible(false);
        this.teacherBox2.setVisible(false);
        this.teacherBox3.setVisible(false);
        this.teacherBox4.setVisible(false);
        this.teacherBox5.setVisible(false);
        this.teacherBox6.setVisible(false);
        this.teacherBox7.setVisible(false);
        this.subjectBox1.setVisible(false);
        this.subjectBox2.setVisible(false);
        this.subjectBox3.setVisible(false);
        this.subjectBox4.setVisible(false);
        this.subjectBox5.setVisible(false);
        this.subjectBox6.setVisible(false);
        this.subjectBox7.setVisible(false);
    }

}
