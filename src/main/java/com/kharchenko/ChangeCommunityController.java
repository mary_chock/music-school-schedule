package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class ChangeCommunityController {

    @FXML
    private TextField name;
    @FXML
    public Button changeCommExitBtn;
    @FXML
    public Button changeCommSaveBtn;
    @FXML
    public Button changeCommAddStudentBtn;
    @FXML
    public Button changeCommDeleteBtn;
    @FXML
    public Button changeCommDeleteStudentBtn1;
    @FXML
    public Button changeCommDeleteStudentBtn2;
    @FXML
    public Button changeCommDeleteStudentBtn3;
    @FXML
    public Button changeCommDeleteStudentBtn4;
    @FXML
    public Button changeCommDeleteStudentBtn5;
    @FXML
    public Button changeCommDeleteStudentBtn6;
    @FXML
    public Button changeCommDeleteStudentBtn7;
    @FXML
    public Button changeCommDeleteStudentBtn8;
    @FXML
    public Button changeCommDeleteStudentBtn9;
    @FXML
    public Button changeCommDeleteStudentBtn10;
    @FXML
    public Button changeCommDeleteStudentBtn11;
    @FXML
    public Button changeCommDeleteStudentBtn12;
    @FXML
    public Button changeCommDeleteStudentBtn13;
    @FXML
    public Button changeCommDeleteStudentBtn14;
    @FXML
    public Button changeCommDeleteStudentBtn15;
    @FXML
    public Button changeCommDeleteStudentBtn16;
    @FXML
    public Button changeCommDeleteStudentBtn17;
    @FXML
    public Button changeCommDeleteStudentBtn18;
    @FXML
    public Button changeCommDeleteStudentBtn19;
    @FXML
    public Button changeCommDeleteStudentBtn20;
    @FXML
    public Button changeCommDeleteStudentBtn21;
    @FXML
    public ChoiceBox<CommunityEntity> communityBox;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox1;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox2;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox3;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox4;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox5;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox6;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox7;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox8;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox9;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox10;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox11;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox12;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox13;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox14;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox15;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox16;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox17;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox18;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox19;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox20;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox21;
    @FXML
    public ChoiceBox<StudentEntity> studentBox;
    @FXML
    public ChoiceBox<StudentEntity> studentBox1;
    @FXML
    public ChoiceBox<StudentEntity> studentBox2;
    @FXML
    public ChoiceBox<StudentEntity> studentBox3;
    @FXML
    public ChoiceBox<StudentEntity> studentBox4;
    @FXML
    public ChoiceBox<StudentEntity> studentBox5;
    @FXML
    public ChoiceBox<StudentEntity> studentBox6;
    @FXML
    public ChoiceBox<StudentEntity> studentBox7;
    @FXML
    public ChoiceBox<StudentEntity> studentBox8;
    @FXML
    public ChoiceBox<StudentEntity> studentBox9;
    @FXML
    public ChoiceBox<StudentEntity> studentBox10;
    @FXML
    public ChoiceBox<StudentEntity> studentBox11;
    @FXML
    public ChoiceBox<StudentEntity> studentBox12;
    @FXML
    public ChoiceBox<StudentEntity> studentBox13;
    @FXML
    public ChoiceBox<StudentEntity> studentBox14;
    @FXML
    public ChoiceBox<StudentEntity> studentBox15;
    @FXML
    public ChoiceBox<StudentEntity> studentBox16;
    @FXML
    public ChoiceBox<StudentEntity> studentBox17;
    @FXML
    public ChoiceBox<StudentEntity> studentBox18;
    @FXML
    public ChoiceBox<StudentEntity> studentBox19;
    @FXML
    public ChoiceBox<StudentEntity> studentBox20;
    @FXML
    public ChoiceBox<StudentEntity> studentBox21;
    @FXML
    public Label classLabel;
    @FXML
    public Label studentLabel;

    private final Resource fxml4;
    private final ApplicationContext applicationContext;
    private final StudentDao studentDao;
    private final TeacherDao teacherDao;
    private final EducationYearDao educationYearDao;
    private final CommunityDao communityDao;
    private final CommunityCompositionDao communityCompositionDao;
    private final GroupCompositionDao groupCompositionDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private final SubjectDao subjectDao;
    private final ScheduleDao scheduleDao;
    private int count;
    private List<ChoiceBox<StudentEntity>> students;
    private List<ChoiceBox<EducationYearEntity>> classes;
    private List<Button> buttons;
    private List<GroupCompositionEntity> groupCompositionEntities;

    public ChangeCommunityController(@Value("classpath:/scenes/main-menu-db.fxml") Resource fxml4,
                                     ApplicationContext applicationContext,
                                     StudentDao studentDao,
                                     TeacherDao teacherDao,
                                     EducationYearDao educationYearDao,
                                     CommunityDao communityDao,
                                     CommunityCompositionDao communityCompositionDao,
                                     GroupCompositionDao groupCompositionDao,
                                     SubjectTeacherDao subjectTeacherDao,
                                     SubjectDao subjectDao,
                                     ScheduleDao scheduleDao) {
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
        this.studentDao = studentDao;
        this.teacherDao = teacherDao;
        this.educationYearDao = educationYearDao;
        this.scheduleDao = scheduleDao;
        this.communityDao = communityDao;
        this.communityCompositionDao = communityCompositionDao;
        this.groupCompositionDao = groupCompositionDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.subjectDao = subjectDao;
    }

    @FXML
    public void initialize() {
        count = 1;
        groupCompositionEntities = groupCompositionDao.getAll();
        initBoxesLists();
        setInitialVisibility();
        setCommunityItems();
        setTeacherItems(teacherBox);
        setClassItems(classBox);
        this.changeCommExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(this.fxml4, changeCommExitBtn);
        });
        ChangeListener<CommunityEntity> changeListener = (observableValue, studentEntity, s1) -> {
            if (s1 != null) {
                setCommunityInfo();
            }
        };
        communityBox.getSelectionModel().selectedItemProperty().addListener(changeListener);
        this.changeCommAddStudentBtn.setOnAction(actionEvent -> addStudent());
        this.studentBox.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox, classBox));
        this.studentBox1.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox1, classBox1));
        this.studentBox2.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox2, classBox2));
        this.studentBox3.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox3, classBox3));
        this.studentBox4.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox4, classBox4));
        this.studentBox5.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox5, classBox5));
        this.studentBox6.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox6, classBox6));
        this.studentBox7.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox7, classBox7));
        this.studentBox8.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox8, classBox8));
        this.studentBox9.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox9, classBox9));
        this.studentBox10.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox10, classBox10));
        this.studentBox11.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox11, classBox11));
        this.studentBox12.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox12, classBox12));
        this.studentBox13.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox13, classBox13));
        this.studentBox14.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox14, classBox14));
        this.studentBox15.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox15, classBox15));
        this.studentBox16.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox16, classBox16));
        this.studentBox17.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox17, classBox17));
        this.studentBox18.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox18, classBox18));
        this.studentBox19.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox19, classBox19));
        this.studentBox20.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox20, classBox20));
        this.studentBox21.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox21, classBox21));
        this.changeCommDeleteStudentBtn1.setOnAction(actionEvent -> clear(classBox1, studentBox1));
        this.changeCommDeleteStudentBtn2.setOnAction(actionEvent -> clear(classBox2, studentBox2));
        this.changeCommDeleteStudentBtn3.setOnAction(actionEvent -> clear(classBox3, studentBox3));
        this.changeCommDeleteStudentBtn4.setOnAction(actionEvent -> clear(classBox4, studentBox4));
        this.changeCommDeleteStudentBtn5.setOnAction(actionEvent -> clear(classBox5, studentBox5));
        this.changeCommDeleteStudentBtn6.setOnAction(actionEvent -> clear(classBox6, studentBox6));
        this.changeCommDeleteStudentBtn7.setOnAction(actionEvent -> clear(classBox7, studentBox7));
        this.changeCommDeleteStudentBtn8.setOnAction(actionEvent -> clear(classBox8, studentBox8));
        this.changeCommDeleteStudentBtn9.setOnAction(actionEvent -> clear(classBox9, studentBox9));
        this.changeCommDeleteStudentBtn10.setOnAction(actionEvent -> clear(classBox10, studentBox10));
        this.changeCommDeleteStudentBtn11.setOnAction(actionEvent -> clear(classBox11, studentBox11));
        this.changeCommDeleteStudentBtn12.setOnAction(actionEvent -> clear(classBox12, studentBox12));
        this.changeCommDeleteStudentBtn13.setOnAction(actionEvent -> clear(classBox13, studentBox13));
        this.changeCommDeleteStudentBtn14.setOnAction(actionEvent -> clear(classBox14, studentBox14));
        this.changeCommDeleteStudentBtn15.setOnAction(actionEvent -> clear(classBox15, studentBox15));
        this.changeCommDeleteStudentBtn16.setOnAction(actionEvent -> clear(classBox16, studentBox16));
        this.changeCommDeleteStudentBtn17.setOnAction(actionEvent -> clear(classBox17, studentBox17));
        this.changeCommDeleteStudentBtn18.setOnAction(actionEvent -> clear(classBox18, studentBox18));
        this.changeCommDeleteStudentBtn19.setOnAction(actionEvent -> clear(classBox19, studentBox19));
        this.changeCommDeleteStudentBtn20.setOnAction(actionEvent -> clear(classBox20, studentBox20));
        this.changeCommDeleteStudentBtn21.setOnAction(actionEvent -> clear(classBox21, studentBox21));
        this.changeCommSaveBtn.setOnAction(actionEvent -> saveCommunity());
        this.changeCommDeleteBtn.setOnAction(actionEvent -> deleteCommunity());
    }

    private boolean showDeleteConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Удаление коллектива");
        alert.setHeaderText("Удалить данные о коллективе '" + communityBox.getValue().toString() + "'?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void deleteCommunity() {
        if (communityBox.getValue() != null)
            if (showDeleteConfirmation()) {
                for (CommunityCompositionEntity communityCompositionEntity : communityCompositionDao.getAll()) {
                    if (communityCompositionEntity.getCommunityCommunityCompositionEntity().getId()
                                                  .equals(communityBox.getValue().getId()))
                        communityCompositionDao.deleteById(communityCompositionEntity.getId());
                }
                for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
                    if(scheduleEntity.getScheduleCommunityEntity().getId().equals(communityBox.getValue().getId()))
                        scheduleDao.deleteById(scheduleEntity.getId());
                }
                communityDao.deleteById(communityBox.getValue().getId());
                changeScene(fxml4, changeCommExitBtn);
            }
    }

    private void setCommunityInfo() {
        if (communityBox.getValue() != null) {
            CommunityEntity communityEntity = communityBox.getValue();
            name.setText(communityEntity.getName());
            ObservableList<TeacherEntity> teachers = setTeacherItems(teacherBox);
            for (TeacherEntity teacher : teachers) {
                if (teacher.getId().equals(communityEntity.getTeacherEntity().getId()))
                    teacherBox.setValue(teacher);
            }
            setCommunityStudents();
        }
    }

    private void setCommunityStudents() {
        classBox.setValue(null);
        studentBox.setValue(null);
        setInitialVisibility();
        for (CommunityCompositionEntity communityCompositionEntity : communityCompositionDao.getAll()) {
            if (communityCompositionEntity.getCommunityCommunityCompositionEntity().getId()
                                          .equals(communityBox.getValue().getId())) {
                addStudent();
                for (int i = 0; i < 22; i++) {
                    if (!checkStudent(classes.get(i), students.get(i))) {
                        ObservableList<EducationYearEntity> years = setClassItems(classes.get(i));
                        for (EducationYearEntity year : years) {
                            if (year.getId().equals(communityCompositionEntity.getStudentCommunityCompositionEntity()
                                                                              .getStudentEducationYearEntity().getId()))
                                classes.get(i).setValue(year);
                        }
                        ObservableList<StudentEntity> studentList = setStudentItems(students.get(i), classes.get(i));
                        for (StudentEntity studentEntity : studentList) {
                            if (studentEntity.getId()
                                             .equals(communityCompositionEntity.getStudentCommunityCompositionEntity()
                                                                               .getId()))
                                students.get(i).setValue(studentEntity);
                        }
                        break;
                    }
                }
            }
        }
    }

    private void setCommunityItems() {
        ObservableList<CommunityEntity> students = FXCollections.observableArrayList();
        students.addAll(communityDao.getAll());
        communityBox.setItems(students);
    }

    private boolean checkStudent(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher) {
        return subject.getValue() != null && teacher.getValue() != null;
    }

    private void saveCommunity() {
        if (checkInput() && teacherBox.getValue() != null)
            if (showSaveConfirmation()) {
                if (checkStudent(classBox, studentBox)) {
                    if (!hasSame()) {
                        CommunityEntity communityEntity = new CommunityEntity();
                        communityEntity.setId(communityBox.getValue().getId());
                        communityEntity.setName(name.getText());
                        communityEntity.setTeacherEntity(teacherBox.getValue());

                        for (CommunityCompositionEntity communityCompositionEntity : communityCompositionDao.getAll()) {
                            if (communityCompositionEntity.getCommunityCommunityCompositionEntity().getId()
                                                          .equals(communityBox.getValue().getId()))
                                communityCompositionDao.deleteById(communityCompositionEntity.getId());
                        }

                        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                            if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId()
                                                    .equals(communityBox.getValue().getTeacherEntity().getId()))
                                subjectTeacherDao.deleteById(subjectTeacherEntity.getId());
                        }

                        SubjectTeacherEntity subjectTeacherEntity = new SubjectTeacherEntity();
                        subjectTeacherEntity.setSubjectTeacherSubjectEntity(subjectDao.getById(51L));
                        subjectTeacherEntity.setTeacherTeacherSubjectEntity(teacherBox.getValue());

                        communityDao.update(communityEntity);
                        subjectTeacherDao.create(subjectTeacherEntity);

                        for (int i = 0; i < 22; i++) {
                            if (checkStudent(classes.get(i), students.get(i))) {
                                CommunityCompositionEntity communityCompositionEntity = new CommunityCompositionEntity();
                                communityCompositionEntity
                                        .setStudentCommunityCompositionEntity(students.get(i).getValue());
                                communityCompositionEntity.setCommunityCommunityCompositionEntity(communityEntity);
                                communityCompositionDao.create(communityCompositionEntity);
                            }
                        }

                        changeScene(fxml4, changeCommSaveBtn);
                    }
                }
            }
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение коллектива");
        alert.setHeaderText("Сохранить новый коллектив " + communityBox.getValue().toString() + "?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean checkInput() {
        String nameRegEx = "^[А-ЯA-Z].+[а-яa-z]$";
        if (!name.getText().isEmpty()) {
            if (!name.getText().matches(nameRegEx)) {
                setAlert("Имя должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
        }
        return true;
    }

    private void setAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Неправильный ввод");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private boolean hasSame() {
        Set<String> strings = new TreeSet<>();
        int counter = 0;
        for (ChoiceBox<StudentEntity> subject : students) {
            if (subject.getValue() != null) {
                strings.add(subject.getValue().toString());
                counter++;
            }
        }
        if (strings.size() < counter) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Вы выбрали одинаковых учеников");
            alert.showAndWait();
            return true;
        }

        return false;
    }

    private void clear(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher) {
        subject.setValue(null);
        teacher.setValue(null);
        subject.setVisible(false);
        teacher.setVisible(false);
        subject.setVisible(true);
        teacher.setVisible(true);
    }

    private void checkSubjectForTeacher(ChoiceBox<StudentEntity> teacher, ChoiceBox<EducationYearEntity> subject) {
        if (subject.getValue() != null)
            setStudentItems(teacher, subject);
    }

    private ObservableList<StudentEntity> setStudentItems(ChoiceBox<StudentEntity> teacher,
                                                          ChoiceBox<EducationYearEntity> subject) {

        ObservableList<StudentEntity> students = FXCollections.observableArrayList();
        for (GroupCompositionEntity groupCompositionEntity : groupCompositionEntities) {
            if (groupCompositionEntity.getGroupGroupCompositionEntity().getGroupEducationYearEntity().getId()
                                      .equals(subject.getValue().getId()))
                students.add(groupCompositionEntity.getStudentGroupCompositionEntity());
        }
        teacher.setItems(students);
        return students;
    }

    private ObservableList<EducationYearEntity> setClassItems(ChoiceBox<EducationYearEntity> year) {
        ObservableList<EducationYearEntity> years = FXCollections.observableArrayList();
        years.addAll(educationYearDao.getAll());
        year.setItems(years);
        return years;
    }

    private ObservableList<TeacherEntity> setTeacherItems(ChoiceBox<TeacherEntity> teacher) {
        ObservableList<TeacherEntity> teachers = FXCollections.observableArrayList();
        teachers.addAll(teacherDao.getAll());
        teacher.setItems(teachers);
        return teachers;
    }

    private void setVisible(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher, Button delete) {
        subject.setVisible(true);
        teacher.setVisible(true);
        delete.setVisible(true);
        count++;
    }

    private void addStudent() {
        switch (count) {
            case 1:
                if (classBox.getValue() != null && studentBox.getValue() != null) {
                    setVisible(classBox1, studentBox1, changeCommDeleteStudentBtn1);
                    setClassItems(classBox1);
                }
                break;
            case 2:
                if (classBox1.getValue() != null && studentBox1.getValue() != null) {
                    setVisible(classBox2, studentBox2, changeCommDeleteStudentBtn2);
                    setClassItems(classBox2);
                }
                break;
            case 3:
                if (classBox2.getValue() != null && studentBox2.getValue() != null) {
                    setVisible(classBox3, studentBox3, changeCommDeleteStudentBtn3);
                    setClassItems(classBox3);
                }
                break;
            case 4:
                if (classBox3.getValue() != null && studentBox3.getValue() != null) {
                    setVisible(classBox4, studentBox4, changeCommDeleteStudentBtn4);
                    setClassItems(classBox4);
                }
                break;
            case 5:
                if (classBox4.getValue() != null && studentBox4.getValue() != null) {
                    setVisible(classBox5, studentBox5, changeCommDeleteStudentBtn5);
                    setClassItems(classBox5);
                }
                break;
            case 6:
                if (classBox5.getValue() != null && studentBox5.getValue() != null) {
                    setVisible(classBox6, studentBox6, changeCommDeleteStudentBtn6);
                    setClassItems(classBox6);
                }
                break;
            case 7:
                if (classBox6.getValue() != null && studentBox6.getValue() != null) {
                    setVisible(classBox7, studentBox7, changeCommDeleteStudentBtn7);
                    setClassItems(classBox7);
                }
                break;
            case 8:
                if (classBox7.getValue() != null && studentBox7.getValue() != null) {
                    setVisible(classBox8, studentBox8, changeCommDeleteStudentBtn8);
                    setClassItems(classBox8);
                }
                break;
            case 9:
                if (classBox8.getValue() != null && studentBox8.getValue() != null) {
                    setVisible(classBox9, studentBox9, changeCommDeleteStudentBtn9);
                    setClassItems(classBox9);
                }
                break;
            case 10:
                if (classBox9.getValue() != null && studentBox9.getValue() != null) {
                    setVisible(classBox10, studentBox10, changeCommDeleteStudentBtn10);
                    setClassItems(classBox10);
                }
                break;
            case 11:
                if (classBox10.getValue() != null && studentBox10.getValue() != null) {
                    setVisible(classBox11, studentBox11, changeCommDeleteStudentBtn11);
                    setClassItems(classBox11);
                    classLabel.setVisible(true);
                    studentLabel.setVisible(true);
                }
                break;
            case 12:
                if (classBox11.getValue() != null && studentBox11.getValue() != null) {
                    setVisible(classBox12, studentBox12, changeCommDeleteStudentBtn12);
                    setClassItems(classBox12);
                }
                break;
            case 13:
                if (classBox12.getValue() != null && studentBox12.getValue() != null) {
                    setVisible(classBox13, studentBox13, changeCommDeleteStudentBtn13);
                    setClassItems(classBox13);
                }
                break;
            case 14:
                if (classBox13.getValue() != null && studentBox13.getValue() != null) {
                    setVisible(classBox14, studentBox14, changeCommDeleteStudentBtn14);
                    setClassItems(classBox14);
                }
                break;
            case 15:
                if (classBox14.getValue() != null && studentBox14.getValue() != null) {
                    setVisible(classBox15, studentBox15, changeCommDeleteStudentBtn15);
                    setClassItems(classBox15);
                }
                break;
            case 16:
                if (classBox15.getValue() != null && studentBox15.getValue() != null) {
                    setVisible(classBox16, studentBox16, changeCommDeleteStudentBtn16);
                    setClassItems(classBox16);
                }
                break;
            case 17:
                if (classBox16.getValue() != null && studentBox16.getValue() != null) {
                    setVisible(classBox17, studentBox17, changeCommDeleteStudentBtn17);
                    setClassItems(classBox17);
                }
                break;
            case 18:
                if (classBox17.getValue() != null && studentBox17.getValue() != null) {
                    setVisible(classBox18, studentBox18, changeCommDeleteStudentBtn18);
                    setClassItems(classBox18);
                }
                break;
            case 19:
                if (classBox18.getValue() != null && studentBox18.getValue() != null) {
                    setVisible(classBox19, studentBox19, changeCommDeleteStudentBtn19);
                    setClassItems(classBox19);
                }
                break;
            case 20:
                if (classBox19.getValue() != null && studentBox19.getValue() != null) {
                    setVisible(classBox20, studentBox20, changeCommDeleteStudentBtn20);
                    setClassItems(classBox20);
                }
                break;
            case 21:
                if (classBox20.getValue() != null && studentBox20.getValue() != null) {
                    setVisible(classBox21, studentBox21, changeCommDeleteStudentBtn21);
                    setClassItems(classBox21);
                }
                break;
        }
    }

    private void initBoxesLists() {
        students = new ArrayList<>();
        classes = new ArrayList<>();
        buttons = new ArrayList<>();
        students.add(studentBox);
        students.add(studentBox1);
        students.add(studentBox2);
        students.add(studentBox3);
        students.add(studentBox4);
        students.add(studentBox5);
        students.add(studentBox6);
        students.add(studentBox7);
        students.add(studentBox8);
        students.add(studentBox9);
        students.add(studentBox10);
        students.add(studentBox11);
        students.add(studentBox12);
        students.add(studentBox13);
        students.add(studentBox14);
        students.add(studentBox15);
        students.add(studentBox16);
        students.add(studentBox17);
        students.add(studentBox18);
        students.add(studentBox19);
        students.add(studentBox20);
        students.add(studentBox21);
        classes.add(classBox);
        classes.add(classBox1);
        classes.add(classBox2);
        classes.add(classBox3);
        classes.add(classBox4);
        classes.add(classBox5);
        classes.add(classBox6);
        classes.add(classBox7);
        classes.add(classBox8);
        classes.add(classBox9);
        classes.add(classBox10);
        classes.add(classBox11);
        classes.add(classBox12);
        classes.add(classBox13);
        classes.add(classBox14);
        classes.add(classBox15);
        classes.add(classBox16);
        classes.add(classBox17);
        classes.add(classBox18);
        classes.add(classBox19);
        classes.add(classBox20);
        classes.add(classBox21);
        buttons.add(changeCommDeleteStudentBtn1);
        buttons.add(changeCommDeleteStudentBtn2);
        buttons.add(changeCommDeleteStudentBtn3);
        buttons.add(changeCommDeleteStudentBtn4);
        buttons.add(changeCommDeleteStudentBtn5);
        buttons.add(changeCommDeleteStudentBtn6);
        buttons.add(changeCommDeleteStudentBtn7);
        buttons.add(changeCommDeleteStudentBtn8);
        buttons.add(changeCommDeleteStudentBtn9);
        buttons.add(changeCommDeleteStudentBtn10);
        buttons.add(changeCommDeleteStudentBtn11);
        buttons.add(changeCommDeleteStudentBtn12);
        buttons.add(changeCommDeleteStudentBtn13);
        buttons.add(changeCommDeleteStudentBtn14);
        buttons.add(changeCommDeleteStudentBtn15);
        buttons.add(changeCommDeleteStudentBtn16);
        buttons.add(changeCommDeleteStudentBtn17);
        buttons.add(changeCommDeleteStudentBtn18);
        buttons.add(changeCommDeleteStudentBtn19);
        buttons.add(changeCommDeleteStudentBtn20);
        buttons.add(changeCommDeleteStudentBtn21);
    }

    private void setInitialVisibility() {
        for (ChoiceBox<StudentEntity> student : students) {
            student.setVisible(false);
        }
        for (ChoiceBox<EducationYearEntity> aClass : classes) {
            aClass.setVisible(false);
        }
        for (Button button : buttons) {
            button.setVisible(false);
        }
        studentBox.setVisible(true);
        classBox.setVisible(true);
        classLabel.setVisible(false);
        studentLabel.setVisible(false);
        count = 1;
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
