package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student_subject")
public class StudentSubjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity studentStudentSubjectEntity;

    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "id")
    private SubjectEntity subjectStudentSubjectEntity;

    @ManyToOne
    @JoinColumn(name = "teacher_subject_id", referencedColumnName = "id")
    private SubjectTeacherEntity teacherSubjectStudentSubjectEntity;

    @OneToMany(mappedBy = "scheduleStudentSubjectEntity")
    private List<ScheduleEntity> scheduleStudentSubjectEntities = new ArrayList<>();

    public SubjectTeacherEntity getTeacherSubjectStudentSubjectEntity() {
        return teacherSubjectStudentSubjectEntity;
    }

    public void setTeacherSubjectStudentSubjectEntity(
            SubjectTeacherEntity teacherSubjectStudentSubjectEntity) {
        this.teacherSubjectStudentSubjectEntity = teacherSubjectStudentSubjectEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentEntity getStudentStudentSubjectEntity() {
        return studentStudentSubjectEntity;
    }

    public void setStudentStudentSubjectEntity(StudentEntity studentStudentSubjectEntity) {
        this.studentStudentSubjectEntity = studentStudentSubjectEntity;
    }

    public SubjectEntity getSubjectStudentSubjectEntity() {
        return subjectStudentSubjectEntity;
    }

    public void setSubjectStudentSubjectEntity(SubjectEntity subjectStudentSubjectEntity) {
        this.subjectStudentSubjectEntity = subjectStudentSubjectEntity;
    }

    public List<ScheduleEntity> getScheduleStudentSubjectEntities() {
        return scheduleStudentSubjectEntities;
    }

    public void setScheduleStudentSubjectEntities(
            List<ScheduleEntity> scheduleStudentSubjectEntities) {
        this.scheduleStudentSubjectEntities = scheduleStudentSubjectEntities;
    }
}