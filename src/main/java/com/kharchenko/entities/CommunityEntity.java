package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "community")
public class CommunityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private TeacherEntity teacherEntity;

    @OneToMany(mappedBy = "communityCommunityCompositionEntity")
    private List<CommunityCompositionEntity> communityCommunityCompositionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "scheduleCommunityEntity")
    private List<ScheduleEntity> communityScheduleEntities = new ArrayList<>();

    public List<CommunityCompositionEntity> getCommunityCommunityCompositionEntities() {
        return communityCommunityCompositionEntities;
    }

    public void setCommunityCommunityCompositionEntities(
            List<CommunityCompositionEntity> communityCommunityCompositionEntities) {
        this.communityCommunityCompositionEntities = communityCommunityCompositionEntities;
    }

    public List<ScheduleEntity> getCommunityScheduleEntities() {
        return communityScheduleEntities;
    }

    public void setCommunityScheduleEntities(List<ScheduleEntity> communityScheduleEntities) {
        this.communityScheduleEntities = communityScheduleEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeacherEntity getTeacherEntity() {
        return teacherEntity;
    }

    public void setTeacherEntity(TeacherEntity teacherEntity) {
        this.teacherEntity = teacherEntity;
    }

    @Override
    public String toString() {
        return name;
    }
}
