package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "teacher")
public class TeacherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "second_name")
    private String secondName;
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;

    @ManyToOne
    @JoinColumn(name = "office_id", referencedColumnName = "id")
    private OfficeEntity officeEntity;

    @OneToMany(mappedBy = "teacherEntity")
    private List<CommunityEntity> communityEntities = new ArrayList<>();

    @OneToMany(mappedBy = "teacherTeacherStudentEntity")
    private List<TeacherStudentEntity> teacherTeacherStudentEntities = new ArrayList<>();

    @OneToMany(mappedBy = "teacherTeacherSubjectEntity")
    private List<SubjectTeacherEntity> teacherTeacherSubjectEntities = new ArrayList<>();

    @OneToMany(mappedBy = "scheduleTeacherEntity")
    private List<ScheduleEntity> teacherScheduleEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public OfficeEntity getOfficeEntity() {
        return officeEntity;
    }

    public void setOfficeEntity(OfficeEntity officeEntity) {
        this.officeEntity = officeEntity;
    }

    public List<CommunityEntity> getCommunityEntities() {
        return communityEntities;
    }

    public void setCommunityEntities(List<CommunityEntity> communityEntities) {
        this.communityEntities = communityEntities;
    }

    public List<TeacherStudentEntity> getTeacherTeacherStudentEntities() {
        return teacherTeacherStudentEntities;
    }

    public void setTeacherTeacherStudentEntities(
            List<TeacherStudentEntity> teacherTeacherStudentEntities) {
        this.teacherTeacherStudentEntities = teacherTeacherStudentEntities;
    }

    public List<SubjectTeacherEntity> getTeacherTeacherSubjectEntities() {
        return teacherTeacherSubjectEntities;
    }

    public void setTeacherTeacherSubjectEntities(
            List<SubjectTeacherEntity> teacherTeacherSubjectEntities) {
        this.teacherTeacherSubjectEntities = teacherTeacherSubjectEntities;
    }

    public List<ScheduleEntity> getTeacherScheduleEntities() {
        return teacherScheduleEntities;
    }

    public void setTeacherScheduleEntities(List<ScheduleEntity> teacherScheduleEntities) {
        this.teacherScheduleEntities = teacherScheduleEntities;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void print(){
        System.out.println(id);
        System.out.println(firstName);
        System.out.println(secondName);
        System.out.println(lastName);
        System.out.println(login);
        System.out.println(password);
    }

    @Override
    public String toString() {
        return firstName+" "+secondName+" "+lastName;
    }
}
