package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "lesson")
public class LessonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "lesson_number")
    private Integer lessonNumber;
    @Column(name = "start_time")
    private String startTime;
    @Column(name = "end_time")
    private String endTime;

    @OneToMany(mappedBy = "scheduleLessonEntity")
    private List<ScheduleEntity> lessonScheduleEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLessonNumber() {
        return lessonNumber;
    }

    public void setLessonNumber(Integer lessonNumber) {
        this.lessonNumber = lessonNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<ScheduleEntity> getLessonScheduleEntities() {
        return lessonScheduleEntities;
    }

    public void setLessonScheduleEntities(List<ScheduleEntity> lessonScheduleEntities) {
        this.lessonScheduleEntities = lessonScheduleEntities;
    }

    @Override
    public String toString() {
        return lessonNumber.toString();
    }
}
