package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "second_name")
    private String secondName;
    @Column(name = "last_name")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "education_year_id", referencedColumnName = "id")
    private EducationYearEntity StudentEducationYearEntity;


    @OneToMany(mappedBy = "studentTeacherStudentEntity")
    private List<TeacherStudentEntity> studentTeacherStudentEntities = new ArrayList<>();

    @OneToMany(mappedBy = "studentCommunityCompositionEntity")
    private List<CommunityCompositionEntity> studentCommunityCompositionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "studentGroupCompositionEntity")
    private List<GroupCompositionEntity> studentGroupCompositionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "studentStudentSubjectEntity")
    private List<StudentSubjectEntity> studentStudentSubjectEntities = new ArrayList<>();

    public List<StudentSubjectEntity> getStudentStudentSubjectEntities() {
        return studentStudentSubjectEntities;
    }

    public void setStudentStudentSubjectEntities(
            List<StudentSubjectEntity> studentStudentSubjectEntities) {
        this.studentStudentSubjectEntities = studentStudentSubjectEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EducationYearEntity getStudentEducationYearEntity() {
        return StudentEducationYearEntity;
    }

    public void setStudentEducationYearEntity(EducationYearEntity studentEducationYearEntity) {
        StudentEducationYearEntity = studentEducationYearEntity;
    }

    public List<TeacherStudentEntity> getStudentTeacherStudentEntities() {
        return studentTeacherStudentEntities;
    }

    public void setStudentTeacherStudentEntities(
            List<TeacherStudentEntity> studentTeacherStudentEntities) {
        this.studentTeacherStudentEntities = studentTeacherStudentEntities;
    }

    public List<CommunityCompositionEntity> getStudentCommunityCompositionEntities() {
        return studentCommunityCompositionEntities;
    }

    public void setStudentCommunityCompositionEntities(
            List<CommunityCompositionEntity> studentCommunityCompositionEntities) {
        this.studentCommunityCompositionEntities = studentCommunityCompositionEntities;
    }

    public List<GroupCompositionEntity> getStudentGroupCompositionEntities() {
        return studentGroupCompositionEntities;
    }

    public void setStudentGroupCompositionEntities(
            List<GroupCompositionEntity> studentGroupCompositionEntities) {
        this.studentGroupCompositionEntities = studentGroupCompositionEntities;
    }

    @Override
    public String toString() {
        if (!lastName.equals("null"))
            return lastName + " " + firstName.charAt(0) + "." + secondName.charAt(0) + ".";
        else
            return "-";
    }
}
