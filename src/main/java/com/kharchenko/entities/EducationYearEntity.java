package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "education_year")
public class EducationYearEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "year_number")
    private Integer yearNumber;

    @OneToMany(mappedBy = "StudentEducationYearEntity")
    private List<StudentEntity> studentEntities = new ArrayList<>();

    @OneToMany(mappedBy = "groupEducationYearEntity")
    private List<GroupEntity> groupEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(Integer yearNumber) {
        this.yearNumber = yearNumber;
    }

    public List<StudentEntity> getStudentEntities() {
        return studentEntities;
    }

    public void setStudentEntities(List<StudentEntity> studentEntities) {
        this.studentEntities = studentEntities;
    }

    public List<GroupEntity> getGroupEntities() {
        return groupEntities;
    }

    public void setGroupEntities(List<GroupEntity> groupEntities) {
        this.groupEntities = groupEntities;
    }

    @Override
    public String toString() {
        if (yearNumber >= 0)
            return String.valueOf(yearNumber);
        else
            return "-";
    }
}
