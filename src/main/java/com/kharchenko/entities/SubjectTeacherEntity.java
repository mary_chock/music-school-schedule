package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "teacher_subject")
public class SubjectTeacherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private TeacherEntity teacherTeacherSubjectEntity;

    @ManyToOne
    @JoinColumn(name = "subject_id", referencedColumnName = "id")
    private SubjectEntity subjectTeacherSubjectEntity;

    @OneToMany(mappedBy = "scheduleTeacherSubjectEntity")
    private List<ScheduleEntity> scheduleTeacherSubjectEntities = new ArrayList<>();

    @OneToMany(mappedBy = "teacherSubjectStudentSubjectEntity")
    private List<StudentSubjectEntity> teacherSubjectStudentSubjectEntities = new ArrayList<>();

    public List<StudentSubjectEntity> getTeacherSubjectStudentSubjectEntities() {
        return teacherSubjectStudentSubjectEntities;
    }

    public void setTeacherSubjectStudentSubjectEntities(
            List<StudentSubjectEntity> teacherSubjectStudentSubjectEntities) {
        this.teacherSubjectStudentSubjectEntities = teacherSubjectStudentSubjectEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TeacherEntity getTeacherTeacherSubjectEntity() {
        return teacherTeacherSubjectEntity;
    }

    public void setTeacherTeacherSubjectEntity(TeacherEntity teacherTeacherSubjectEntity) {
        this.teacherTeacherSubjectEntity = teacherTeacherSubjectEntity;
    }

    public SubjectEntity getSubjectTeacherSubjectEntity() {
        return subjectTeacherSubjectEntity;
    }

    public void setSubjectTeacherSubjectEntity(SubjectEntity subjectTeacherSubjectEntity) {
        this.subjectTeacherSubjectEntity = subjectTeacherSubjectEntity;
    }

    public List<ScheduleEntity> getScheduleTeacherSubjectEntities() {
        return scheduleTeacherSubjectEntities;
    }

    public void setScheduleTeacherSubjectEntities(
            List<ScheduleEntity> scheduleTeacherSubjectEntities) {
        this.scheduleTeacherSubjectEntities = scheduleTeacherSubjectEntities;
    }
}
