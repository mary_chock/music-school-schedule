package com.kharchenko.entities;

import com.kharchenko.enums.WeekDay;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "schedule")
public class ScheduleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private Date endDate;
    @Column(name = "week_day")
    private String weekDay;

    @ManyToOne
    @JoinColumn(name = "teacher_subject_id", referencedColumnName = "id")
    private SubjectTeacherEntity scheduleTeacherSubjectEntity;

    @ManyToOne
    @JoinColumn(name = "student_subject_id", referencedColumnName = "id")
    private StudentSubjectEntity scheduleStudentSubjectEntity;

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private TeacherEntity scheduleTeacherEntity;

    @ManyToOne
    @JoinColumn(name = "teacher_student_id", referencedColumnName = "id")
    private TeacherStudentEntity scheduleTeacherStudentEntity;

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private GroupEntity scheduleGroupEntity;

    @ManyToOne
    @JoinColumn(name = "community_id", referencedColumnName = "id")
    private CommunityEntity scheduleCommunityEntity;

    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "id")
    private LessonEntity scheduleLessonEntity;

    public String getStudentInfo() {
        String subject;
        if (scheduleTeacherSubjectEntity.getSubjectTeacherSubjectEntity().getId() == 51)
            subject = ""+scheduleTeacherSubjectEntity.getSubjectTeacherSubjectEntity().getName();
        else
            subject = scheduleTeacherSubjectEntity.getSubjectTeacherSubjectEntity().getName();
        String teacher = scheduleTeacherEntity.getLastName()+" "+scheduleTeacherEntity.getFirstName().charAt(0)+"."+scheduleTeacherEntity.getSecondName().charAt(0)+".";
        String office = scheduleTeacherEntity.getOfficeEntity().toString()+" каб.";
        return teacher + "\n" + subject + "\n" + office;
    }

    public String getInfo() {
        String subject = scheduleTeacherSubjectEntity.getSubjectTeacherSubjectEntity().getName();
        String community;
        if (scheduleCommunityEntity != null)
            community = "" + scheduleCommunityEntity.getName();
        else
            community = "";
        String group;
        if (scheduleGroupEntity != null)
            group = "Класс " + scheduleGroupEntity.toString();
        else
            group = "";
        String student;
        if (scheduleStudentSubjectEntity != null) {
            StudentEntity studentEntity = scheduleStudentSubjectEntity.getStudentStudentSubjectEntity();
            student = studentEntity.getLastName() + " " + studentEntity.getFirstName().charAt(0) + "." + studentEntity
                    .getSecondName().charAt(0) + ".";
        } else
            student = "";
        return subject + "\n " + community + "\n " + group + "\n " + student;
    }

    public StudentSubjectEntity getScheduleStudentSubjectEntity() {
        return scheduleStudentSubjectEntity;
    }

    public void setScheduleStudentSubjectEntity(StudentSubjectEntity scheduleStudentSubjectEntity) {
        this.scheduleStudentSubjectEntity = scheduleStudentSubjectEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public SubjectTeacherEntity getScheduleTeacherSubjectEntity() {
        return scheduleTeacherSubjectEntity;
    }

    public void setScheduleTeacherSubjectEntity(SubjectTeacherEntity scheduleTeacherSubjectEntity) {
        this.scheduleTeacherSubjectEntity = scheduleTeacherSubjectEntity;
    }

    public TeacherEntity getScheduleTeacherEntity() {
        return scheduleTeacherEntity;
    }

    public void setScheduleTeacherEntity(TeacherEntity scheduleTeacherEntity) {
        this.scheduleTeacherEntity = scheduleTeacherEntity;
    }

    public TeacherStudentEntity getScheduleTeacherStudentEntity() {
        return scheduleTeacherStudentEntity;
    }

    public void setScheduleTeacherStudentEntity(TeacherStudentEntity scheduleTeacherStudentEntity) {
        this.scheduleTeacherStudentEntity = scheduleTeacherStudentEntity;
    }

    public GroupEntity getScheduleGroupEntity() {
        return scheduleGroupEntity;
    }

    public void setScheduleGroupEntity(GroupEntity scheduleGroupEntity) {
        this.scheduleGroupEntity = scheduleGroupEntity;
    }

    public CommunityEntity getScheduleCommunityEntity() {
        return scheduleCommunityEntity;
    }

    public void setScheduleCommunityEntity(CommunityEntity scheduleCommunityEntity) {
        this.scheduleCommunityEntity = scheduleCommunityEntity;
    }

    public LessonEntity getScheduleLessonEntity() {
        return scheduleLessonEntity;
    }

    public void setScheduleLessonEntity(LessonEntity scheduleLessonEntity) {
        this.scheduleLessonEntity = scheduleLessonEntity;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = "%15s%18s%22s%22s%17s%23s%17s%25s";
        String subject = scheduleTeacherSubjectEntity.getSubjectTeacherSubjectEntity().getName();
        String start = sdf.format(startDate);
        String end = sdf.format(endDate);
        String weekDay = getWeekDay();
        String lesson = scheduleLessonEntity.getLessonNumber().toString();
        String community;
        if (scheduleCommunityEntity != null)
            community = scheduleCommunityEntity.getName();
        else
            community = "--------";
        String group;
        if (scheduleGroupEntity != null)
            group = scheduleGroupEntity.toString();
        else
            group = "--------";
        String student;
        if (scheduleStudentSubjectEntity != null) {
            StudentEntity studentEntity = scheduleStudentSubjectEntity.getStudentStudentSubjectEntity();
            student = studentEntity.getLastName() + " " + studentEntity.getFirstName().charAt(0) + "." + studentEntity
                    .getSecondName().charAt(0) + ".";
        } else
            student = "--------";
        String result = String.format(format, subject, start, end, weekDay, lesson, community, group, student);
        return result;
    }
}
