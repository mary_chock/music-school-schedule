package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student_group")
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "education_year_id", referencedColumnName = "id")
    private EducationYearEntity groupEducationYearEntity;

    @OneToMany(mappedBy = "groupGroupCompositionEntity")
    private List<GroupCompositionEntity> groupGroupCompositionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "scheduleGroupEntity")
    private List<ScheduleEntity> groupScheduleEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public List<ScheduleEntity> getGroupScheduleEntities() {
        return groupScheduleEntities;
    }

    public void setGroupScheduleEntities(List<ScheduleEntity> groupScheduleEntities) {
        this.groupScheduleEntities = groupScheduleEntities;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EducationYearEntity getGroupEducationYearEntity() {
        return groupEducationYearEntity;
    }

    public void setGroupEducationYearEntity(EducationYearEntity groupEducationYearEntity) {
        this.groupEducationYearEntity = groupEducationYearEntity;
    }

    public List<GroupCompositionEntity> getGroupGroupCompositionEntities() {
        return groupGroupCompositionEntities;
    }

    public void setGroupGroupCompositionEntities(
            List<GroupCompositionEntity> groupGroupCompositionEntities) {
        this.groupGroupCompositionEntities = groupGroupCompositionEntities;
    }

    @Override
    public String toString() {
        return getGroupEducationYearEntity().getYearNumber().toString();
    }
}
