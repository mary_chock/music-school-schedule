package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "teacher_student")
public class TeacherStudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private TeacherEntity teacherTeacherStudentEntity;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity studentTeacherStudentEntity;

    @OneToMany(mappedBy = "scheduleTeacherStudentEntity")
    private List<ScheduleEntity> teacherStudentScheduleEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TeacherEntity getTeacherTeacherStudentEntity() {
        return teacherTeacherStudentEntity;
    }

    public void setTeacherTeacherStudentEntity(TeacherEntity teacherTeacherStudentEntity) {
        this.teacherTeacherStudentEntity = teacherTeacherStudentEntity;
    }

    public StudentEntity getStudentTeacherStudentEntity() {
        return studentTeacherStudentEntity;
    }

    public void setStudentTeacherStudentEntity(StudentEntity studentTeacherStudentEntity) {
        this.studentTeacherStudentEntity = studentTeacherStudentEntity;
    }

    public List<ScheduleEntity> getTeacherStudentScheduleEntities() {
        return teacherStudentScheduleEntities;
    }

    public void setTeacherStudentScheduleEntities(
            List<ScheduleEntity> teacherStudentScheduleEntities) {
        this.teacherStudentScheduleEntities = teacherStudentScheduleEntities;
    }
}
