package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "group_composition")
public class GroupCompositionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private GroupEntity groupGroupCompositionEntity;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity studentGroupCompositionEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GroupEntity getGroupGroupCompositionEntity() {
        return groupGroupCompositionEntity;
    }

    public void setGroupGroupCompositionEntity(GroupEntity groupGroupCompositionEntity) {
        this.groupGroupCompositionEntity = groupGroupCompositionEntity;
    }

    public StudentEntity getStudentGroupCompositionEntity() {
        return studentGroupCompositionEntity;
    }

    public void setStudentGroupCompositionEntity(StudentEntity studentGroupCompositionEntity) {
        this.studentGroupCompositionEntity = studentGroupCompositionEntity;
    }

}
