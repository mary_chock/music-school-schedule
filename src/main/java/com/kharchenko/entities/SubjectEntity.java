package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subject")
public class SubjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "work_type_id", referencedColumnName = "id")
    private WorkTypeEntity workTypeEntity;

    @OneToMany(mappedBy = "subjectTeacherSubjectEntity")
    private List<SubjectTeacherEntity> subjectTeacherSubjectEntities = new ArrayList<>();

    @OneToMany(mappedBy = "subjectStudentSubjectEntity")
    private List<StudentSubjectEntity> subjectStudentSubjectEntities = new ArrayList<>();

    public List<StudentSubjectEntity> getSubjectStudentSubjectEntities() {
        return subjectStudentSubjectEntities;
    }

    public void setSubjectStudentSubjectEntities(
            List<StudentSubjectEntity> subjectStudentSubjectEntities) {
        this.subjectStudentSubjectEntities = subjectStudentSubjectEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkTypeEntity getWorkTypeEntity() {
        return workTypeEntity;
    }

    public void setWorkTypeEntity(WorkTypeEntity workTypeEntity) {
        this.workTypeEntity = workTypeEntity;
    }

    public List<SubjectTeacherEntity> getSubjectTeacherSubjectEntities() {
        return subjectTeacherSubjectEntities;
    }

    public void setSubjectTeacherSubjectEntities(
            List<SubjectTeacherEntity> subjectTeacherSubjectEntities) {
        this.subjectTeacherSubjectEntities = subjectTeacherSubjectEntities;
    }

    @Override
    public String toString() {
        return name;
    }


}
