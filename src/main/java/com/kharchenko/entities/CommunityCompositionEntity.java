package com.kharchenko.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "community_composition")
public class CommunityCompositionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "community_id", referencedColumnName = "id")
    private CommunityEntity communityCommunityCompositionEntity;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity studentCommunityCompositionEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CommunityEntity getCommunityCommunityCompositionEntity() {
        return communityCommunityCompositionEntity;
    }

    public void setCommunityCommunityCompositionEntity(
            CommunityEntity communityCommunityCompositionEntity) {
        this.communityCommunityCompositionEntity = communityCommunityCompositionEntity;
    }

    public StudentEntity getStudentCommunityCompositionEntity() {
        return studentCommunityCompositionEntity;
    }

    public void setStudentCommunityCompositionEntity(StudentEntity studentCommunityCompositionEntity) {
        this.studentCommunityCompositionEntity = studentCommunityCompositionEntity;
    }
}
