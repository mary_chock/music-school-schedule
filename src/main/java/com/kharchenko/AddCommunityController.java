package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class AddCommunityController {

    @FXML
    private TextField name;
    @FXML
    public Button newCommExitBtn;
    @FXML
    public Button newCommSaveBtn;
    @FXML
    public Button newCommAddStudentBtn;
    @FXML
    public Button newCommDeleteStudentBtn1;
    @FXML
    public Button newCommDeleteStudentBtn2;
    @FXML
    public Button newCommDeleteStudentBtn3;
    @FXML
    public Button newCommDeleteStudentBtn4;
    @FXML
    public Button newCommDeleteStudentBtn5;
    @FXML
    public Button newCommDeleteStudentBtn6;
    @FXML
    public Button newCommDeleteStudentBtn7;
    @FXML
    public Button newCommDeleteStudentBtn8;
    @FXML
    public Button newCommDeleteStudentBtn9;
    @FXML
    public Button newCommDeleteStudentBtn10;
    @FXML
    public Button newCommDeleteStudentBtn11;
    @FXML
    public Button newCommDeleteStudentBtn12;
    @FXML
    public Button newCommDeleteStudentBtn13;
    @FXML
    public Button newCommDeleteStudentBtn14;
    @FXML
    public Button newCommDeleteStudentBtn15;
    @FXML
    public Button newCommDeleteStudentBtn16;
    @FXML
    public Button newCommDeleteStudentBtn17;
    @FXML
    public Button newCommDeleteStudentBtn18;
    @FXML
    public Button newCommDeleteStudentBtn19;
    @FXML
    public Button newCommDeleteStudentBtn20;
    @FXML
    public Button newCommDeleteStudentBtn21;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox1;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox2;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox3;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox4;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox5;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox6;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox7;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox8;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox9;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox10;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox11;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox12;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox13;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox14;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox15;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox16;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox17;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox18;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox19;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox20;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox21;
    @FXML
    public ChoiceBox<StudentEntity> studentBox;
    @FXML
    public ChoiceBox<StudentEntity> studentBox1;
    @FXML
    public ChoiceBox<StudentEntity> studentBox2;
    @FXML
    public ChoiceBox<StudentEntity> studentBox3;
    @FXML
    public ChoiceBox<StudentEntity> studentBox4;
    @FXML
    public ChoiceBox<StudentEntity> studentBox5;
    @FXML
    public ChoiceBox<StudentEntity> studentBox6;
    @FXML
    public ChoiceBox<StudentEntity> studentBox7;
    @FXML
    public ChoiceBox<StudentEntity> studentBox8;
    @FXML
    public ChoiceBox<StudentEntity> studentBox9;
    @FXML
    public ChoiceBox<StudentEntity> studentBox10;
    @FXML
    public ChoiceBox<StudentEntity> studentBox11;
    @FXML
    public ChoiceBox<StudentEntity> studentBox12;
    @FXML
    public ChoiceBox<StudentEntity> studentBox13;
    @FXML
    public ChoiceBox<StudentEntity> studentBox14;
    @FXML
    public ChoiceBox<StudentEntity> studentBox15;
    @FXML
    public ChoiceBox<StudentEntity> studentBox16;
    @FXML
    public ChoiceBox<StudentEntity> studentBox17;
    @FXML
    public ChoiceBox<StudentEntity> studentBox18;
    @FXML
    public ChoiceBox<StudentEntity> studentBox19;
    @FXML
    public ChoiceBox<StudentEntity> studentBox20;
    @FXML
    public ChoiceBox<StudentEntity> studentBox21;
    @FXML
    public Label classLabel;
    @FXML
    public Label studentLabel;

    private final Resource fxml4;
    private final ApplicationContext applicationContext;
    private final StudentDao studentDao;
    private final TeacherDao teacherDao;
    private final EducationYearDao educationYearDao;
    private final CommunityDao communityDao;
    private final CommunityCompositionDao communityCompositionDao;
    private final GroupCompositionDao groupCompositionDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private final SubjectDao subjectDao;
    private int count;
    private List<ChoiceBox<StudentEntity>> students;
    private List<ChoiceBox<EducationYearEntity>> classes;
    private List<Button> buttons;
    private List<GroupCompositionEntity> groupCompositionEntities;

    public AddCommunityController(@Value("classpath:/scenes/main-menu-db.fxml") Resource fxml4,
                                  ApplicationContext applicationContext,
                                  StudentDao studentDao,
                                  TeacherDao teacherDao,
                                  EducationYearDao educationYearDao,
                                  CommunityDao communityDao,
                                  CommunityCompositionDao communityCompositionDao,
                                  GroupCompositionDao groupCompositionDao,
                                  SubjectTeacherDao subjectTeacherDao,
                                  SubjectDao subjectDao) {
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
        this.studentDao = studentDao;
        this.teacherDao = teacherDao;
        this.educationYearDao = educationYearDao;
        this.communityDao = communityDao;
        this.communityCompositionDao = communityCompositionDao;
        this.groupCompositionDao = groupCompositionDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.subjectDao = subjectDao;
    }

    @FXML
    public void initialize() {
        count = 1;
        groupCompositionEntities = groupCompositionDao.getAll();
        initBoxesLists();
        setInitialVisibility();
        setTeacherItems(teacherBox);
        setClassItems(classBox);
        this.newCommExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(this.fxml4, newCommExitBtn);
        });
        this.newCommAddStudentBtn.setOnAction(actionEvent -> addStudent());
        this.studentBox.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox, classBox));
        this.studentBox1.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox1, classBox1));
        this.studentBox2.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox2, classBox2));
        this.studentBox3.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox3, classBox3));
        this.studentBox4.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox4, classBox4));
        this.studentBox5.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox5, classBox5));
        this.studentBox6.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox6, classBox6));
        this.studentBox7.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox7, classBox7));
        this.studentBox8.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox8, classBox8));
        this.studentBox9.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox9, classBox9));
        this.studentBox10.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox10, classBox10));
        this.studentBox11.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox11, classBox11));
        this.studentBox12.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox12, classBox12));
        this.studentBox13.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox13, classBox13));
        this.studentBox14.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox14, classBox14));
        this.studentBox15.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox15, classBox15));
        this.studentBox16.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox16, classBox16));
        this.studentBox17.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox17, classBox17));
        this.studentBox18.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox18, classBox18));
        this.studentBox19.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox19, classBox19));
        this.studentBox20.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox20, classBox20));
        this.studentBox21.setOnMouseClicked(mouseEvent -> checkSubjectForTeacher(studentBox21, classBox21));
        this.newCommDeleteStudentBtn1.setOnAction(actionEvent -> clear(classBox1, studentBox1));
        this.newCommDeleteStudentBtn2.setOnAction(actionEvent -> clear(classBox2, studentBox2));
        this.newCommDeleteStudentBtn3.setOnAction(actionEvent -> clear(classBox3, studentBox3));
        this.newCommDeleteStudentBtn4.setOnAction(actionEvent -> clear(classBox4, studentBox4));
        this.newCommDeleteStudentBtn5.setOnAction(actionEvent -> clear(classBox5, studentBox5));
        this.newCommDeleteStudentBtn6.setOnAction(actionEvent -> clear(classBox6, studentBox6));
        this.newCommDeleteStudentBtn7.setOnAction(actionEvent -> clear(classBox7, studentBox7));
        this.newCommDeleteStudentBtn8.setOnAction(actionEvent -> clear(classBox8, studentBox8));
        this.newCommDeleteStudentBtn9.setOnAction(actionEvent -> clear(classBox9, studentBox9));
        this.newCommDeleteStudentBtn10.setOnAction(actionEvent -> clear(classBox10, studentBox10));
        this.newCommDeleteStudentBtn11.setOnAction(actionEvent -> clear(classBox11, studentBox11));
        this.newCommDeleteStudentBtn12.setOnAction(actionEvent -> clear(classBox12, studentBox12));
        this.newCommDeleteStudentBtn13.setOnAction(actionEvent -> clear(classBox13, studentBox13));
        this.newCommDeleteStudentBtn14.setOnAction(actionEvent -> clear(classBox14, studentBox14));
        this.newCommDeleteStudentBtn15.setOnAction(actionEvent -> clear(classBox15, studentBox15));
        this.newCommDeleteStudentBtn16.setOnAction(actionEvent -> clear(classBox16, studentBox16));
        this.newCommDeleteStudentBtn17.setOnAction(actionEvent -> clear(classBox17, studentBox17));
        this.newCommDeleteStudentBtn18.setOnAction(actionEvent -> clear(classBox18, studentBox18));
        this.newCommDeleteStudentBtn19.setOnAction(actionEvent -> clear(classBox19, studentBox19));
        this.newCommDeleteStudentBtn20.setOnAction(actionEvent -> clear(classBox20, studentBox20));
        this.newCommDeleteStudentBtn21.setOnAction(actionEvent -> clear(classBox21, studentBox21));
        this.newCommSaveBtn.setOnAction(actionEvent -> saveTeacher());
    }

    private boolean checkSubject(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher) {
        return subject.getValue() != null && teacher.getValue() != null;
    }

    private void saveTeacher() {
        if (checkInput() && teacherBox.getValue() != null)
            if (showSaveConfirmation()) {
                if (checkSubject(classBox, studentBox)) {
                    if (!hasSame()) {
                        CommunityEntity communityEntity = new CommunityEntity();
                        communityEntity.setName(name.getText());
                        communityEntity.setTeacherEntity(teacherBox.getValue());

                        CommunityCompositionEntity communityCompositionEntity1 = new CommunityCompositionEntity();
                        communityCompositionEntity1.setStudentCommunityCompositionEntity(studentBox.getValue());
                        communityCompositionEntity1.setCommunityCommunityCompositionEntity(communityEntity);

                        SubjectTeacherEntity subjectTeacherEntity = new SubjectTeacherEntity();
                        subjectTeacherEntity.setSubjectTeacherSubjectEntity(subjectDao.getById(51L));
                        subjectTeacherEntity.setTeacherTeacherSubjectEntity(teacherBox.getValue());

                        communityDao.create(communityEntity);
                        communityCompositionDao.create(communityCompositionEntity1);
                        subjectTeacherDao.create(subjectTeacherEntity);

                        for (int i = 0; i < 21; i++) {
                            if (checkSubject(classes.get(i), students.get(i))) {
                                CommunityCompositionEntity communityCompositionEntity = new CommunityCompositionEntity();
                                communityCompositionEntity
                                        .setStudentCommunityCompositionEntity(students.get(i).getValue());
                                communityCompositionEntity.setCommunityCommunityCompositionEntity(communityEntity);
                                communityCompositionDao.create(communityCompositionEntity);
                            }
                        }

                        changeScene(fxml4, newCommSaveBtn);
                    }
                }
            }
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение коллектива");
        alert.setHeaderText("Сохранить новый коллектив?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean checkInput() {
        String nameRegEx = "^[А-ЯA-Z].+[а-яa-z]$";
        if (!name.getText().isEmpty()) {
            if (!name.getText().matches(nameRegEx)) {
                setAlert("Имя должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
        }
        return true;
    }

    private void setAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Неправильный ввод");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private boolean hasSame() {
        Set<String> strings = new TreeSet<>();
        strings.add(studentBox.getValue().toString());
        int counter = 1;
        for (ChoiceBox<StudentEntity> subject : students) {
            if (subject.getValue() != null) {
                strings.add(subject.getValue().toString());
                counter++;
            }
        }
        if (strings.size() < counter) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Вы выбрали одинаковых учеников");
            alert.showAndWait();
            return true;
        }

        return false;
    }

    private void clear(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher) {
        subject.setValue(null);
        teacher.setValue(null);
        subject.setVisible(false);
        teacher.setVisible(false);
        subject.setVisible(true);
        teacher.setVisible(true);
    }

    private void checkSubjectForTeacher(ChoiceBox<StudentEntity> teacher, ChoiceBox<EducationYearEntity> subject) {
        if (subject.getValue() != null)
            setTeacherItems(teacher, subject);
    }

    private void setTeacherItems(ChoiceBox<StudentEntity> teacher, ChoiceBox<EducationYearEntity> subject) {

        ObservableList<StudentEntity> students = FXCollections.observableArrayList();
        for (GroupCompositionEntity groupCompositionEntity : groupCompositionEntities) {
            if (groupCompositionEntity.getGroupGroupCompositionEntity().getGroupEducationYearEntity().getId()
                                      .equals(subject.getValue().getId()))
                students.add(groupCompositionEntity.getStudentGroupCompositionEntity());
        }
        teacher.setItems(students);
    }

    private void setClassItems(ChoiceBox<EducationYearEntity> year) {
        ObservableList<EducationYearEntity> years = FXCollections.observableArrayList();
        years.addAll(educationYearDao.getAll());
        year.setItems(years);
    }

    private void setTeacherItems(ChoiceBox<TeacherEntity> teacher) {
        ObservableList<TeacherEntity> teachers = FXCollections.observableArrayList();
        teachers.addAll(teacherDao.getAll());
        teacher.setItems(teachers);
    }

    private void setVisible(ChoiceBox<EducationYearEntity> subject, ChoiceBox<StudentEntity> teacher, Button delete) {
        subject.setVisible(true);
        teacher.setVisible(true);
        delete.setVisible(true);
        count++;
    }

    private void addStudent() {
        switch (count) {
            case 1:
                if (classBox.getValue() != null && studentBox.getValue() != null) {
                    setVisible(classBox1, studentBox1, newCommDeleteStudentBtn1);
                    setClassItems(classBox1);
                }
                break;
            case 2:
                if (classBox1.getValue() != null && studentBox1.getValue() != null) {
                    setVisible(classBox2, studentBox2, newCommDeleteStudentBtn2);
                    setClassItems(classBox2);
                }
                break;
            case 3:
                if (classBox2.getValue() != null && studentBox2.getValue() != null) {
                    setVisible(classBox3, studentBox3, newCommDeleteStudentBtn3);
                    setClassItems(classBox3);
                }
                break;
            case 4:
                if (classBox3.getValue() != null && studentBox3.getValue() != null) {
                    setVisible(classBox4, studentBox4, newCommDeleteStudentBtn4);
                    setClassItems(classBox4);
                }
                break;
            case 5:
                if (classBox4.getValue() != null && studentBox4.getValue() != null) {
                    setVisible(classBox5, studentBox5, newCommDeleteStudentBtn5);
                    setClassItems(classBox5);
                }
                break;
            case 6:
                if (classBox5.getValue() != null && studentBox5.getValue() != null) {
                    setVisible(classBox6, studentBox6, newCommDeleteStudentBtn6);
                    setClassItems(classBox6);
                }
                break;
            case 7:
                if (classBox6.getValue() != null && studentBox6.getValue() != null) {
                    setVisible(classBox7, studentBox7, newCommDeleteStudentBtn7);
                    setClassItems(classBox7);
                }
                break;
            case 8:
                if (classBox7.getValue() != null && studentBox7.getValue() != null) {
                    setVisible(classBox8, studentBox8, newCommDeleteStudentBtn8);
                    setClassItems(classBox8);
                }
                break;
            case 9:
                if (classBox8.getValue() != null && studentBox8.getValue() != null) {
                    setVisible(classBox9, studentBox9, newCommDeleteStudentBtn9);
                    setClassItems(classBox9);
                }
                break;
            case 10:
                if (classBox9.getValue() != null && studentBox9.getValue() != null) {
                    setVisible(classBox10, studentBox10, newCommDeleteStudentBtn10);
                    setClassItems(classBox10);
                }
                break;
            case 11:
                if (classBox10.getValue() != null && studentBox10.getValue() != null) {
                    setVisible(classBox11, studentBox11, newCommDeleteStudentBtn11);
                    setClassItems(classBox11);
                    classLabel.setVisible(true);
                    studentLabel.setVisible(true);
                }
                break;
            case 12:
                if (classBox11.getValue() != null && studentBox11.getValue() != null) {
                    setVisible(classBox12, studentBox12, newCommDeleteStudentBtn12);
                    setClassItems(classBox12);
                }
                break;
            case 13:
                if (classBox12.getValue() != null && studentBox12.getValue() != null) {
                    setVisible(classBox13, studentBox13, newCommDeleteStudentBtn13);
                    setClassItems(classBox13);
                }
                break;
            case 14:
                if (classBox13.getValue() != null && studentBox13.getValue() != null) {
                    setVisible(classBox14, studentBox14, newCommDeleteStudentBtn14);
                    setClassItems(classBox14);
                }
                break;
            case 15:
                if (classBox14.getValue() != null && studentBox14.getValue() != null) {
                    setVisible(classBox15, studentBox15, newCommDeleteStudentBtn15);
                    setClassItems(classBox15);
                }
                break;
            case 16:
                if (classBox15.getValue() != null && studentBox15.getValue() != null) {
                    setVisible(classBox16, studentBox16, newCommDeleteStudentBtn16);
                    setClassItems(classBox16);
                }
                break;
            case 17:
                if (classBox16.getValue() != null && studentBox16.getValue() != null) {
                    setVisible(classBox17, studentBox17, newCommDeleteStudentBtn17);
                    setClassItems(classBox17);
                }
                break;
            case 18:
                if (classBox17.getValue() != null && studentBox17.getValue() != null) {
                    setVisible(classBox18, studentBox18, newCommDeleteStudentBtn18);
                    setClassItems(classBox18);
                }
                break;
            case 19:
                if (classBox18.getValue() != null && studentBox18.getValue() != null) {
                    setVisible(classBox19, studentBox19, newCommDeleteStudentBtn19);
                    setClassItems(classBox19);
                }
                break;
            case 20:
                if (classBox19.getValue() != null && studentBox19.getValue() != null) {
                    setVisible(classBox20, studentBox20, newCommDeleteStudentBtn20);
                    setClassItems(classBox20);
                }
                break;
            case 21:
                if (classBox20.getValue() != null && studentBox20.getValue() != null) {
                    setVisible(classBox21, studentBox21, newCommDeleteStudentBtn21);
                    setClassItems(classBox21);
                }
                break;
        }
    }

    private void initBoxesLists() {
        students = new ArrayList<>();
        classes = new ArrayList<>();
        buttons = new ArrayList<>();
        students.add(studentBox1);
        students.add(studentBox2);
        students.add(studentBox3);
        students.add(studentBox4);
        students.add(studentBox5);
        students.add(studentBox6);
        students.add(studentBox7);
        students.add(studentBox8);
        students.add(studentBox9);
        students.add(studentBox10);
        students.add(studentBox11);
        students.add(studentBox12);
        students.add(studentBox13);
        students.add(studentBox14);
        students.add(studentBox15);
        students.add(studentBox16);
        students.add(studentBox17);
        students.add(studentBox18);
        students.add(studentBox19);
        students.add(studentBox20);
        students.add(studentBox21);
        classes.add(classBox1);
        classes.add(classBox2);
        classes.add(classBox3);
        classes.add(classBox4);
        classes.add(classBox5);
        classes.add(classBox6);
        classes.add(classBox7);
        classes.add(classBox8);
        classes.add(classBox9);
        classes.add(classBox10);
        classes.add(classBox11);
        classes.add(classBox12);
        classes.add(classBox13);
        classes.add(classBox14);
        classes.add(classBox15);
        classes.add(classBox16);
        classes.add(classBox17);
        classes.add(classBox18);
        classes.add(classBox19);
        classes.add(classBox20);
        classes.add(classBox21);
        buttons.add(newCommDeleteStudentBtn1);
        buttons.add(newCommDeleteStudentBtn2);
        buttons.add(newCommDeleteStudentBtn3);
        buttons.add(newCommDeleteStudentBtn4);
        buttons.add(newCommDeleteStudentBtn5);
        buttons.add(newCommDeleteStudentBtn6);
        buttons.add(newCommDeleteStudentBtn7);
        buttons.add(newCommDeleteStudentBtn8);
        buttons.add(newCommDeleteStudentBtn9);
        buttons.add(newCommDeleteStudentBtn10);
        buttons.add(newCommDeleteStudentBtn11);
        buttons.add(newCommDeleteStudentBtn12);
        buttons.add(newCommDeleteStudentBtn13);
        buttons.add(newCommDeleteStudentBtn14);
        buttons.add(newCommDeleteStudentBtn15);
        buttons.add(newCommDeleteStudentBtn16);
        buttons.add(newCommDeleteStudentBtn17);
        buttons.add(newCommDeleteStudentBtn18);
        buttons.add(newCommDeleteStudentBtn19);
        buttons.add(newCommDeleteStudentBtn20);
        buttons.add(newCommDeleteStudentBtn21);
    }

    private void setInitialVisibility() {
        for (ChoiceBox<StudentEntity> student : students) {
            student.setVisible(false);
        }
        for (ChoiceBox<EducationYearEntity> aClass : classes) {
            aClass.setVisible(false);
        }
        for (Button button : buttons) {
            button.setVisible(false);
        }
        classLabel.setVisible(false);
        studentLabel.setVisible(false);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
