package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.ScheduleDao;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class DeleteScheduleController {

    @FXML
    public Button deleteSchedExitBtn;
    @FXML
    public ListView<String> list;
    @FXML
    public ListView<String> list1;
    @FXML
    public ListView<String> list2;
    @FXML
    public ListView<String> list3;
    @FXML
    public ListView<String> list4;
    @FXML
    public ListView<String> list5;
    @FXML
    public ListView<String> list6;
    @FXML
    public ListView<String> list7;

    private final Resource fxml1;
    private final ApplicationContext applicationContext;
    private final ScheduleDao scheduleDao;

    public DeleteScheduleController(@Value("classpath:/scenes/teacher-schedule.fxml") Resource fxml1,
                                    ApplicationContext applicationContext,
                                    ScheduleDao scheduleDao) {
        this.fxml1 = fxml1;
        this.applicationContext = applicationContext;
        this.scheduleDao = scheduleDao;
    }

    @FXML
    public void initialize() {
        setScheduleItems();
        onIndexListener(list);
        onIndexListener(list1);
        onIndexListener(list2);
        onIndexListener(list3);
        onIndexListener(list4);
        onIndexListener(list5);
        onIndexListener(list6);
        onIndexListener(list7);
        deleteSchedExitBtn.setOnAction(actionEvent -> changeScene(fxml1, deleteSchedExitBtn));

    }

    private void onIndexListener(ListView<String> list){
        list.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        list.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                deleteSchedule(newValue);
                setScheduleItems();
            }
        });
    }

    private void deleteSchedule(Number index) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (showDeleteConfirmation()){
            for(int i=0;;i++){
                if(i==index.intValue()){
                    for (ScheduleEntity entity : scheduleDao.getAll()) {
                        if(entity.getScheduleTeacherSubjectEntity().getSubjectTeacherSubjectEntity().toString().equals(String.valueOf(list.getItems().get(i)))
                        && sdf.format(entity.getStartDate()).equals(String.valueOf(list1.getItems().get(i)))
                        && sdf.format(entity.getEndDate()).equals(String.valueOf(list2.getItems().get(i)))
                        && entity.getWeekDay().equals(String.valueOf(list3.getItems().get(i)))
                        && entity.getScheduleLessonEntity().toString().equals(String.valueOf(list4.getItems().get(i))))
                            if((list5.getItems().get(i).equals("-") && entity.getScheduleCommunityEntity()==null) || entity.getScheduleCommunityEntity().toString().equals(list5.getItems().get(i)))
                                if((list6.getItems().get(i).equals("-") && entity.getScheduleGroupEntity()==null) || entity.getScheduleGroupEntity().toString().equals(list6.getItems().get(i)))
                                    if((list7.getItems().get(i).equals("-") && entity.getScheduleStudentSubjectEntity()==null) || entity.getScheduleStudentSubjectEntity().getStudentStudentSubjectEntity().toString().equals(list7.getItems().get(i))) {
                                        scheduleDao.deleteById(entity.getId());
                                        break;
                                    }
                    }
                }
                if(i>index.intValue())
                    break;
            }


        }

    }

    private boolean showDeleteConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Удаление");
        alert.setHeaderText("Удалить выбранную запись?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void setScheduleItems() {
        List<ScheduleEntity> schedules = new ArrayList<>();
        for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
            if (scheduleEntity.getScheduleTeacherEntity().getId()
                              .equals(AuthorizationController.getLoggedTeacher().getId())) {
                schedules.add(scheduleEntity);
            }
        }
        ObservableList<String> students = FXCollections.observableArrayList();
        ObservableList<String> starts = FXCollections.observableArrayList();
        ObservableList<String> ends = FXCollections.observableArrayList();
        ObservableList<String> weeks = FXCollections.observableArrayList();
        ObservableList<String> lessons = FXCollections.observableArrayList();
        ObservableList<String> communities = FXCollections.observableArrayList();
        ObservableList<String> groups = FXCollections.observableArrayList();
        ObservableList<String> subjects = FXCollections.observableArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (ScheduleEntity schedule : schedules) {
            starts.add(sdf.format(schedule.getStartDate()));
            ends.add(sdf.format(schedule.getEndDate()));
            weeks.add(schedule.getWeekDay());
            lessons.add(schedule.getScheduleLessonEntity().toString());
            if (schedule.getScheduleCommunityEntity() != null)
                communities.add(schedule.getScheduleCommunityEntity().toString());
            else
                communities.add("-");
            if (schedule.getScheduleGroupEntity() != null)
                groups.add(schedule.getScheduleGroupEntity().toString());
            else
                groups.add("-");
            subjects.add(schedule.getScheduleTeacherSubjectEntity().getSubjectTeacherSubjectEntity().toString());
            if (schedule.getScheduleStudentSubjectEntity() != null)
                students.add(schedule.getScheduleStudentSubjectEntity().getStudentStudentSubjectEntity().toString());
            else
                students.add("-");
        }
        list.setItems(subjects);
        list1.setItems(starts);
        list2.setItems(ends);
        list3.setItems(weeks);
        list4.setItems(lessons);
        list5.setItems(communities);
        list6.setItems(groups);
        list7.setItems(students);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
