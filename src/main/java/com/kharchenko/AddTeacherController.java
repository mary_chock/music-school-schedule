package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class AddTeacherController {

    @FXML
    public Button newTeacherExitBtn;
    @FXML
    public Button newTeacherSaveBtn;
    @FXML
    public Button newTeacherAddSubjectBtn;
    @FXML
    public Button newTeacherDeleteSubjectBtn1;
    @FXML
    public Button newTeacherDeleteSubjectBtn2;
    @FXML
    public Button newTeacherDeleteSubjectBtn3;
    @FXML
    public Button newTeacherDeleteSubjectBtn4;
    @FXML
    public Button newTeacherDeleteSubjectBtn5;
    @FXML
    public Button newTeacherDeleteSubjectBtn6;
    @FXML
    public Button newTeacherDeleteSubjectBtn7;
    @FXML
    public TextField firstName;
    @FXML
    public TextField secondName;
    @FXML
    public TextField lastName;
    @FXML
    public TextField login;
    @FXML
    public TextField password;
    @FXML
    public ChoiceBox<OfficeEntity> officeNumberBox;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox1;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox2;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox3;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox4;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox5;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox6;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox7;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox1;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox2;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox3;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox4;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox5;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox6;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox7;

    private final Resource fxml4;
    private final ApplicationContext applicationContext;
    private final WorkTypeDao workTypeDao;
    private final SubjectDao subjectDao;
    private final OfficeDao officeDao;
    private final TeacherDao teacherDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private List<SubjectEntity> subjectEntities;
    private int count;
    private List<ChoiceBox<WorkTypeEntity>> works;
    private List<ChoiceBox<SubjectEntity>> subjects;

    public AddTeacherController(@Value("classpath:/scenes/main-menu-db.fxml") Resource fxml4,
                                ApplicationContext applicationContext,
                                WorkTypeDao workTypeDao,
                                SubjectDao subjectDao,
                                OfficeDao officeDao,
                                TeacherDao teacherDao,
                                SubjectTeacherDao subjectTeacherDao) {
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
        this.workTypeDao = workTypeDao;
        this.subjectDao = subjectDao;
        this.officeDao = officeDao;
        this.teacherDao = teacherDao;
        this.subjectTeacherDao = subjectTeacherDao;
    }

    @FXML
    public void initialize() {
        count = 1;
        subjectEntities = subjectDao.getAll();
        setInitialVisibility();
        initBoxesLists();
        this.newTeacherExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(this.fxml4, newTeacherExitBtn);
        });
        this.newTeacherAddSubjectBtn.setOnAction(actionEvent -> addSubject());
        setWorkItems(workTypeBox);
        setOfficeItems(officeNumberBox);
        this.subjectBox.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox, workTypeBox));
        this.subjectBox1.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox1, workTypeBox1));
        this.subjectBox2.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox2, workTypeBox2));
        this.subjectBox3.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox3, workTypeBox3));
        this.subjectBox4.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox4, workTypeBox4));
        this.subjectBox5.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox5, workTypeBox5));
        this.subjectBox6.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox6, workTypeBox6));
        this.subjectBox7.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox7, workTypeBox7));
        this.newTeacherDeleteSubjectBtn1
                .setOnAction(actionEvent -> clear(workTypeBox1, subjectBox1));
        this.newTeacherDeleteSubjectBtn2
                .setOnAction(actionEvent -> clear(workTypeBox2, subjectBox2));
        this.newTeacherDeleteSubjectBtn3
                .setOnAction(actionEvent -> clear(workTypeBox3, subjectBox3));
        this.newTeacherDeleteSubjectBtn4
                .setOnAction(actionEvent -> clear(workTypeBox4, subjectBox4));
        this.newTeacherDeleteSubjectBtn5
                .setOnAction(actionEvent -> clear(workTypeBox5, subjectBox5));
        this.newTeacherDeleteSubjectBtn6
                .setOnAction(actionEvent -> clear(workTypeBox6, subjectBox6));
        this.newTeacherDeleteSubjectBtn7
                .setOnAction(actionEvent -> clear(workTypeBox7, subjectBox7));
        this.newTeacherSaveBtn.setOnAction(actionEvent -> saveTeacher());
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение преподавателя");
        alert.setHeaderText("Сохранить нового преподавателя?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void initBoxesLists() {
        works = new ArrayList<>();
        subjects = new ArrayList<>();
        works.add(workTypeBox1);
        works.add(workTypeBox2);
        works.add(workTypeBox3);
        works.add(workTypeBox4);
        works.add(workTypeBox5);
        works.add(workTypeBox6);
        works.add(workTypeBox7);
        subjects.add(subjectBox1);
        subjects.add(subjectBox2);
        subjects.add(subjectBox3);
        subjects.add(subjectBox4);
        subjects.add(subjectBox5);
        subjects.add(subjectBox6);
        subjects.add(subjectBox7);
    }

    private void saveTeacher() {
        if (checkInput() && officeNumberBox.getValue() != null)
            if (showSaveConfirmation()) {
                if (checkSubject(workTypeBox, subjectBox)) {
                    if (!hasSame()) {
                        TeacherEntity teacherEntity = new TeacherEntity();
                        teacherEntity.setFirstName(firstName.getText());
                        teacherEntity.setSecondName(secondName.getText());
                        teacherEntity.setLastName(lastName.getText());
                        teacherEntity.setOfficeEntity(officeNumberBox.getValue());
                        teacherEntity.setLogin(login.getText());
                        teacherEntity.setPassword(password.getText());

                        SubjectTeacherEntity subjectTeacherEntity = new SubjectTeacherEntity();
                        subjectTeacherEntity.setTeacherTeacherSubjectEntity(teacherEntity);
                        subjectTeacherEntity.setSubjectTeacherSubjectEntity(subjectBox.getValue());

                        teacherDao.create(teacherEntity);
                        subjectTeacherDao.create(subjectTeacherEntity);

                        for (int i = 0; i < 7; i++) {
                            if (checkSubject(works.get(i), subjects.get(i))) {
                                SubjectTeacherEntity subjectTeacherEntity1 = new SubjectTeacherEntity();
                                subjectTeacherEntity1.setTeacherTeacherSubjectEntity(teacherEntity);
                                subjectTeacherEntity1.setSubjectTeacherSubjectEntity(subjects.get(i).getValue());
                                subjectTeacherDao.create(subjectTeacherEntity1);
                            }
                        }

                        changeScene(fxml4, newTeacherExitBtn);
                    }
                }
            }
    }

    private boolean hasSame() {
        Set<String> strings = new TreeSet<>();
        strings.add(subjectBox.getValue().getName());
        int counter = 1;
        for (ChoiceBox<SubjectEntity> subject : subjects) {
            if (subject.getValue() != null) {
                strings.add(subject.getValue().getName());
                counter++;
            }
        }
        if (strings.size() < counter) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Вы выбрали одинаковые дисциплины");
            alert.showAndWait();
            return true;
        }

        return false;
    }

    private boolean checkSubject(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject) {
        return workType.getValue() != null && subject.getValue() != null;
    }

    private boolean checkInput() {
        String nameRegEx = "^[А-Я].+[а-я]$";
        String loginRegEx = "^[a-zA-Z]{3,}$";
        String passRegEx = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        if (!firstName.getText().isEmpty()
                && !secondName.getText().isEmpty()
                && !lastName.getText().isEmpty()
                && !login.getText().isEmpty()
                && !password.getText().isEmpty()) {

            if (!firstName.getText().matches(nameRegEx)) {
                setAlert("Имя должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!secondName.getText().matches(nameRegEx)) {
                setAlert("Отчество должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!lastName.getText().matches(nameRegEx)) {
                setAlert("Фамилия должна начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!login.getText().matches(loginRegEx)) {
                setAlert("Логин должен состоять минимум из 3 английских символов");
                return false;
            }
            if (!password.getText().matches(passRegEx)) {
                setAlert("Пароль должен содержать цифры, заглавные и строчные английские символы. длина не меньше 8");
                return false;
            }

        }
        return true;
    }

    private void setAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Неправильный ввод");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void checkWorkForSubject(ChoiceBox<SubjectEntity> subject, ChoiceBox<WorkTypeEntity> workType) {
        if (workType.getValue() != null)
            setSubjectItems(subject, workType);
    }

    private void setSubjectItems(ChoiceBox<SubjectEntity> subject, ChoiceBox<WorkTypeEntity> workType) {

        ObservableList<SubjectEntity> subjects = FXCollections.observableArrayList();
        for (SubjectEntity subjectEntity : subjectEntities) {
            if (!subjectEntity.getName().equals("Коллектив"))
                if (workType.getValue().getName()
                            .equals(String.valueOf(subjectEntity.getWorkTypeEntity().getName())))
                    subjects.add(subjectEntity);
        }
        subject.setItems(subjects);
    }

    private void setOfficeItems(ChoiceBox<OfficeEntity> office) {
        ObservableList<OfficeEntity> offices = FXCollections.observableArrayList();
        offices.addAll(officeDao.getAll());
        office.setItems(offices);
    }

    private void setWorkItems(ChoiceBox<WorkTypeEntity> workType) {
        ObservableList<WorkTypeEntity> workTypes = FXCollections.observableArrayList();
        for (WorkTypeEntity workTypeEntity : workTypeDao.getAll()) {
            if (!workTypeEntity.getName().equals("Коллективная"))
                workTypes.add(workTypeEntity);

        }
        workType.setItems(workTypes);
    }

    private void addSubject() {
        switch (count) {
            case 1:
                if (workTypeBox.getValue() != null && subjectBox.getValue() != null) {
                    setVisible(workTypeBox1, subjectBox1, newTeacherDeleteSubjectBtn1);
                    setWorkItems(workTypeBox1);
                }
                break;
            case 2:
                if (workTypeBox1.getValue() != null && subjectBox1.getValue() != null) {
                    setVisible(workTypeBox2, subjectBox2, newTeacherDeleteSubjectBtn2);
                    setWorkItems(workTypeBox2);
                }
                break;
            case 3:
                if (workTypeBox2.getValue() != null && subjectBox2.getValue() != null) {
                    setVisible(workTypeBox3, subjectBox3, newTeacherDeleteSubjectBtn3);
                    setWorkItems(workTypeBox3);
                }
                break;
            case 4:
                if (workTypeBox3.getValue() != null && subjectBox3.getValue() != null) {
                    setVisible(workTypeBox4, subjectBox4, newTeacherDeleteSubjectBtn4);
                    setWorkItems(workTypeBox4);
                }
                break;
            case 5:
                if (workTypeBox4.getValue() != null && subjectBox4.getValue() != null) {
                    setVisible(workTypeBox5, subjectBox5, newTeacherDeleteSubjectBtn5);
                    setWorkItems(workTypeBox5);
                }
                break;
            case 6:
                if (workTypeBox5.getValue() != null && subjectBox5.getValue() != null) {
                    setVisible(workTypeBox6, subjectBox6, newTeacherDeleteSubjectBtn6);
                    setWorkItems(workTypeBox6);
                }
                break;
            case 7:
                if (workTypeBox6.getValue() != null && subjectBox6.getValue() != null) {
                    setVisible(workTypeBox7, subjectBox7, newTeacherDeleteSubjectBtn7);
                    setWorkItems(workTypeBox7);
                }
                break;
        }
    }

    private void setVisible(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject, Button delete) {
        workType.setVisible(true);
        subject.setVisible(true);
        delete.setVisible(true);
        count++;
    }

    private void clear(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject) {
        workType.setValue(null);
        subject.setValue(null);
        workType.setVisible(false);
        subject.setVisible(false);
        workType.setVisible(true);
        subject.setVisible(true);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setInitialVisibility() {
        this.newTeacherDeleteSubjectBtn1.setVisible(false);
        this.newTeacherDeleteSubjectBtn2.setVisible(false);
        this.newTeacherDeleteSubjectBtn3.setVisible(false);
        this.newTeacherDeleteSubjectBtn4.setVisible(false);
        this.newTeacherDeleteSubjectBtn5.setVisible(false);
        this.newTeacherDeleteSubjectBtn6.setVisible(false);
        this.newTeacherDeleteSubjectBtn7.setVisible(false);
        this.workTypeBox1.setVisible(false);
        this.workTypeBox2.setVisible(false);
        this.workTypeBox3.setVisible(false);
        this.workTypeBox4.setVisible(false);
        this.workTypeBox5.setVisible(false);
        this.workTypeBox6.setVisible(false);
        this.workTypeBox7.setVisible(false);
        this.subjectBox1.setVisible(false);
        this.subjectBox2.setVisible(false);
        this.subjectBox3.setVisible(false);
        this.subjectBox4.setVisible(false);
        this.subjectBox5.setVisible(false);
        this.subjectBox6.setVisible(false);
        this.subjectBox7.setVisible(false);
    }

}
