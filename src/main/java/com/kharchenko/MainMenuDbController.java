package com.kharchenko;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class MainMenuDbController {

    @FXML
    public Button addNewNoteBtn;
    @FXML
    public Button changeNoteBtn;

    private final Resource fxml1;
    private final Resource fxml2;
    private final ApplicationContext applicationContext;

    public MainMenuDbController(@Value("classpath:/scenes/choose-to-add.fxml") Resource fxml1,
                                @Value("classpath:/scenes/choose-to-change.fxml") Resource fxml2,
                                ApplicationContext ac) {
        this.fxml1 = fxml1;
        this.fxml2 = fxml2;
        this.applicationContext = ac;
    }

    @FXML
    public void initialize(){
        this.addNewNoteBtn.setOnAction(actionEvent -> {
            try {
                Stage stage = (Stage) addNewNoteBtn.getScene().getWindow();
                URL url = this.fxml1.getURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                fxmlLoader.setControllerFactory(applicationContext::getBean);
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        this.changeNoteBtn.setOnAction(actionEvent -> {
            try {
                Stage stage = (Stage) changeNoteBtn.getScene().getWindow();
                URL url = this.fxml2.getURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                fxmlLoader.setControllerFactory(applicationContext::getBean);
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
