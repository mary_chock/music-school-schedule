package com.kharchenko.services;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TestService {

    private final CommonDao<EducationYearEntity> educationYearDao;
    private final CommonDao<OfficeEntity> officeDao;
    private final CommonDao<WorkTypeEntity> workTypeDao;
    private final CommonDao<StudentEntity> studentDao;
    private final CommonDao<TeacherEntity> teacherDao;
    private final CommonDao<SubjectEntity> subjectDao;
    private final CommonDao<LessonEntity> lessonDao;
    private final CommonDao<TeacherStudentEntity> teacherStudentDao;
    private final CommonDao<CommunityEntity> communityDao;
    private final CommonDao<CommunityCompositionEntity> communityCompositionDao;
    private final CommonDao<GroupEntity> groupDao;
    private final CommonDao<GroupCompositionEntity> groupCompositionDao;
    private final CommonDao<SubjectTeacherEntity> subjectTeacherDao;
    private final CommonDao<ScheduleEntity> scheduleDao;

    public TestService(EducationYearDao educationYearDao, OfficeDao officeDao,
                       WorkTypeDao workTypeDao, StudentDao studentDao, TeacherDao teacherDao,
                       SubjectDao subjectDao, LessonDao lessonDao,
                       TeacherStudentDao teacherStudentDao, CommunityDao communityDao,
                       CommunityCompositionDao communityCompositionDao, GroupDao groupDao,
                       GroupCompositionDao groupCompositionDao,
                       SubjectTeacherDao subjectTeacherDao, ScheduleDao scheduleDao) {

        this.educationYearDao = educationYearDao;
        this.officeDao = officeDao;
        this.workTypeDao = workTypeDao;
        this.studentDao = studentDao;
        this.teacherDao = teacherDao;
        this.subjectDao = subjectDao;
        this.lessonDao = lessonDao;
        this.teacherStudentDao = teacherStudentDao;
        this.communityDao = communityDao;
        this.communityCompositionDao = communityCompositionDao;
        this.groupDao = groupDao;
        this.groupCompositionDao = groupCompositionDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.scheduleDao = scheduleDao;
    }

    public void fillStaticTables(List<EducationYearEntity> educationYearEntity, List<OfficeEntity> officeEntity,
                                 List<WorkTypeEntity> workTypeEntity,List<SubjectEntity> subjectEntity,
                                 List<LessonEntity> lessonEntity){

        educationYearDao.createSeveral(educationYearEntity);
        officeDao.createSeveral(officeEntity);
        workTypeDao.createSeveral(workTypeEntity);
        subjectDao.createSeveral(subjectEntity);
        lessonDao.createSeveral(lessonEntity);

    }

    public void addCommSubj(){
        SubjectEntity subjectEntity = new SubjectEntity();
        subjectEntity.setName("Коллектив");
        subjectEntity.setWorkTypeEntity(workTypeDao.getById(16L));
        subjectDao.create(subjectEntity);
    }

    public void deletecommunities(){
        for (CommunityEntity communityEntity : communityDao.getAll()) {
            communityDao.deleteById(communityEntity.getId());
        }
    }

    public void addGroups(){
        for (EducationYearEntity educationYearEntity : educationYearDao.getAll()) {
            GroupEntity groupEntity = new GroupEntity();
            groupEntity.setGroupEducationYearEntity(educationYearEntity);
            groupDao.create(groupEntity);
        }
    }

    public void addAdmin(){
        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setFirstName("Ольга");
        teacherEntity.setSecondName("Михайловна");
        teacherEntity.setLastName("Коротко");
        teacherEntity.setLogin("admin");
        teacherEntity.setPassword("admin");
        OfficeEntity officeEntity = new OfficeEntity();
        for (OfficeEntity office : officeDao.getAll()) {
            if(office.getOfficeNumber()==2) {
                officeEntity = office;
                break;
            }
        }
        teacherEntity.setOfficeEntity(officeEntity);
        teacherDao.create(teacherEntity);
    }

    public void deleteComm(){
        for (SubjectEntity subjectEntity : subjectDao.getAll()) {
            if(subjectEntity.getWorkTypeEntity().getName().equals("Коллективная"))
                subjectDao.deleteById(subjectEntity.getId());
        }
    }

    public void cleanAll(){
        for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
            scheduleDao.deleteById(scheduleEntity.getId());
        }
        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
            subjectTeacherDao.deleteById(subjectTeacherEntity.getId());
        }
        for (GroupCompositionEntity groupCompositionEntity : groupCompositionDao.getAll()) {
            groupCompositionDao.deleteById(groupCompositionEntity.getId());
        }
        for (GroupEntity groupEntity : groupDao.getAll()) {
            groupDao.deleteById(groupEntity.getId());
        }
        for (CommunityCompositionEntity communityCompositionEntity : communityCompositionDao.getAll()) {
            communityCompositionDao.deleteById(communityCompositionEntity.getId());
        }
        for (CommunityEntity communityEntity : communityDao.getAll()) {
            communityDao.deleteById(communityEntity.getId());
        }
        for (LessonEntity lessonEntity : lessonDao.getAll()) {
            lessonDao.deleteById(lessonEntity.getId());
        }
        for (SubjectEntity subjectEntity : subjectDao.getAll()) {
            subjectDao.deleteById(subjectEntity.getId());
        }
        for (TeacherStudentEntity teacherStudentEntity : teacherStudentDao.getAll()) {
            teacherStudentDao.deleteById(teacherStudentEntity.getId());
        }
        for (TeacherEntity teacherEntity : teacherDao.getAll()) {
            teacherDao.deleteById(teacherEntity.getId());
        }
        for (StudentEntity studentEntity : studentDao.getAll()) {
            studentDao.deleteById(studentEntity.getId());
        }
        for (WorkTypeEntity workTypeEntity : workTypeDao.getAll()) {
            workTypeDao.deleteById(workTypeEntity.getId());
        }
        for (OfficeEntity officeEntity : officeDao.getAll()) {
            officeDao.deleteById(officeEntity.getId());
        }
        for (EducationYearEntity educationYearEntity : educationYearDao.getAll()) {
            educationYearDao.deleteById(educationYearEntity.getId());
        }
    }

    public void startTestService(EducationYearEntity educationYearEntity, OfficeEntity officeEntity,
                                 WorkTypeEntity workTypeEntity, StudentEntity studentEntity,
                                 TeacherEntity teacherEntity, SubjectEntity subjectEntity,
                                 LessonEntity lessonEntity, TeacherStudentEntity teacherStudentEntity,
                                 CommunityEntity communityEntity, CommunityCompositionEntity communityCompositionEntity,
                                 GroupEntity groupEntity, GroupCompositionEntity groupCompositionEntity,
                                 SubjectTeacherEntity subjectTeacherEntity, ScheduleEntity scheduleEntity){

        educationYearDao.create(educationYearEntity);
        officeDao.create(officeEntity);
        workTypeDao.create(workTypeEntity);
        studentDao.create(studentEntity);
        teacherDao.create(teacherEntity);
        subjectDao.create(subjectEntity);
        lessonDao.create(lessonEntity);
        teacherStudentDao.create(teacherStudentEntity);
        communityDao.create(communityEntity);
        communityCompositionDao.create(communityCompositionEntity);
        groupDao.create(groupEntity);
        groupCompositionDao.create(groupCompositionEntity);
        subjectTeacherDao.create(subjectTeacherEntity);
        scheduleDao.create(scheduleEntity);

    }
}
