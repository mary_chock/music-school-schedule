package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.TeacherEntity;
import com.kharchenko.entities.WorkTypeEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class WorkTypeDao extends HibernateCommonDao<WorkTypeEntity>{

    private SessionFactory sessionFactory;

    public WorkTypeDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<WorkTypeEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<WorkTypeEntity> cq = cb.createQuery(WorkTypeEntity.class);
        Root<WorkTypeEntity> rootEntry = cq.from(WorkTypeEntity.class);
        CriteriaQuery<WorkTypeEntity> all = cq.select(rootEntry);

        TypedQuery<WorkTypeEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public WorkTypeEntity create(WorkTypeEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create work type", sessionFactory);
    }

    public List<WorkTypeEntity> createSeveral(List<WorkTypeEntity> entities){
        for (WorkTypeEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public WorkTypeEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(WorkTypeEntity.class, id),
                                       "Can't get work type", sessionFactory);
    }

    @Override
    public WorkTypeEntity update(WorkTypeEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update work type", sessionFactory);
    }

    @Override
    public boolean delete(WorkTypeEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete work type", sessionFactory);
    }

    public WorkTypeEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            WorkTypeEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete work type with ID: " + id + " from database", sessionFactory);
    }
}
