package com.kharchenko.services.dao;

import com.kharchenko.entities.StudentSubjectEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class StudentSubjectDao extends HibernateCommonDao<StudentSubjectEntity>{

    private SessionFactory sessionFactory;

    public StudentSubjectDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<StudentSubjectEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<StudentSubjectEntity> cq = cb.createQuery(StudentSubjectEntity.class);
        Root<StudentSubjectEntity> rootEntry = cq.from(StudentSubjectEntity.class);
        CriteriaQuery<StudentSubjectEntity> all = cq.select(rootEntry);

        TypedQuery<StudentSubjectEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public StudentSubjectEntity create(StudentSubjectEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create student-subject composition", sessionFactory);
    }

    public List<StudentSubjectEntity> createSeveral(List<StudentSubjectEntity> entities){
        for (StudentSubjectEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public StudentSubjectEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(StudentSubjectEntity.class, id),
                                       "Can't get student-subject composition", sessionFactory);
    }

    @Override
    public StudentSubjectEntity update(StudentSubjectEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update student-subject composition", sessionFactory);
    }

    @Override
    public boolean delete(StudentSubjectEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete student-subject composition", sessionFactory);
    }

    public StudentSubjectEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            StudentSubjectEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete student-subject composition with ID: " + id + " from database", sessionFactory);
    }
}