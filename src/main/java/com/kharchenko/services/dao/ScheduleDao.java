package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.ScheduleEntity;
import com.kharchenko.entities.StudentEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class ScheduleDao extends HibernateCommonDao<ScheduleEntity>{

    private SessionFactory sessionFactory;

    public ScheduleDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<ScheduleEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ScheduleEntity> cq = cb.createQuery(ScheduleEntity.class);
        Root<ScheduleEntity> rootEntry = cq.from(ScheduleEntity.class);
        CriteriaQuery<ScheduleEntity> all = cq.select(rootEntry);

        TypedQuery<ScheduleEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public ScheduleEntity create(ScheduleEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create schedule", sessionFactory);
    }

    public List<ScheduleEntity> createSeveral(List<ScheduleEntity> entities){
        for (ScheduleEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public ScheduleEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(ScheduleEntity.class, id),
                                       "Can't get schedule", sessionFactory);
    }

    @Override
    public ScheduleEntity update(ScheduleEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update schedule", sessionFactory);
    }

    @Override
    public boolean delete(ScheduleEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete schedule", sessionFactory);
    }

    public ScheduleEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            ScheduleEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete schedule with ID: " + id + " from database", sessionFactory);
    }
}
