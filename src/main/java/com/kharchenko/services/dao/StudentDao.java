package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.StudentEntity;
import com.kharchenko.entities.SubjectEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class StudentDao extends HibernateCommonDao<StudentEntity>{

    private SessionFactory sessionFactory;

    public StudentDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<StudentEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<StudentEntity> cq = cb.createQuery(StudentEntity.class);
        Root<StudentEntity> rootEntry = cq.from(StudentEntity.class);
        CriteriaQuery<StudentEntity> all = cq.select(rootEntry);

        TypedQuery<StudentEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public StudentEntity create(StudentEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create student", sessionFactory);
    }

    public List<StudentEntity> createSeveral(List<StudentEntity> entities){
        for (StudentEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public StudentEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(StudentEntity.class, id),
                                       "Can't get student", sessionFactory);
    }

    @Override
    public StudentEntity update(StudentEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update student", sessionFactory);
    }

    @Override
    public boolean delete(StudentEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete student", sessionFactory);
    }

    public StudentEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            StudentEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete student with ID: " + id + " from database", sessionFactory);
    }
}
