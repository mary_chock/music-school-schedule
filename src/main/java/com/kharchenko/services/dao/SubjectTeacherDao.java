package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.SubjectTeacherEntity;
import com.kharchenko.entities.TeacherEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class SubjectTeacherDao extends HibernateCommonDao<SubjectTeacherEntity>{

    private SessionFactory sessionFactory;

    public SubjectTeacherDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SubjectTeacherEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<SubjectTeacherEntity> cq = cb.createQuery(SubjectTeacherEntity.class);
        Root<SubjectTeacherEntity> rootEntry = cq.from(SubjectTeacherEntity.class);
        CriteriaQuery<SubjectTeacherEntity> all = cq.select(rootEntry);

        TypedQuery<SubjectTeacherEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public SubjectTeacherEntity create(SubjectTeacherEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create teacher-subject composition", sessionFactory);
    }

    public List<SubjectTeacherEntity> createSeveral(List<SubjectTeacherEntity> entities){
        for (SubjectTeacherEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public SubjectTeacherEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(SubjectTeacherEntity.class, id),
                                       "Can't get teacher-subject composition", sessionFactory);
    }

    @Override
    public SubjectTeacherEntity update(SubjectTeacherEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update teacher-subject composition", sessionFactory);
    }

    @Override
    public boolean delete(SubjectTeacherEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete teacher-subject composition", sessionFactory);
    }

    public SubjectTeacherEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            SubjectTeacherEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete teacher-subject composition with ID: " + id + " from database", sessionFactory);
    }
}
