package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.CommunityEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class CommunityCompositionDao extends HibernateCommonDao<CommunityCompositionEntity>{

    private SessionFactory sessionFactory;

    public CommunityCompositionDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CommunityCompositionEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<CommunityCompositionEntity> cq = cb.createQuery(CommunityCompositionEntity.class);
        Root<CommunityCompositionEntity> rootEntry = cq.from(CommunityCompositionEntity.class);
        CriteriaQuery<CommunityCompositionEntity> all = cq.select(rootEntry);

        TypedQuery<CommunityCompositionEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public CommunityCompositionEntity create(CommunityCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create community composition", sessionFactory);
    }

    @Override
    public List<CommunityCompositionEntity> createSeveral(List<CommunityCompositionEntity> entities){
        for (CommunityCompositionEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public CommunityCompositionEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(CommunityCompositionEntity.class, id),
                                       "Can't get community composition", sessionFactory);
    }

    @Override
    public CommunityCompositionEntity update(CommunityCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update community composition", sessionFactory);
    }

    @Override
    public boolean delete(CommunityCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete community composition", sessionFactory);
    }

    public CommunityCompositionEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            CommunityCompositionEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete community composition with ID: " + id + " from database", sessionFactory);
    }
}
