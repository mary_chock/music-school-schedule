package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.LessonEntity;
import com.kharchenko.entities.OfficeEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class LessonDao extends HibernateCommonDao<LessonEntity>{

    private SessionFactory sessionFactory;

    public LessonDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<LessonEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<LessonEntity> cq = cb.createQuery(LessonEntity.class);
        Root<LessonEntity> rootEntry = cq.from(LessonEntity.class);
        CriteriaQuery<LessonEntity> all = cq.select(rootEntry);

        TypedQuery<LessonEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public LessonEntity create(LessonEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create lesson", sessionFactory);
    }

    public List<LessonEntity> createSeveral(List<LessonEntity> entities){
        for (LessonEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public LessonEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(LessonEntity.class, id),
                                       "Can't get lesson", sessionFactory);
    }

    @Override
    public LessonEntity update(LessonEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update lesson", sessionFactory);
    }

    @Override
    public boolean delete(LessonEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete lesson", sessionFactory);
    }

    public LessonEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            LessonEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete lesson with ID: " + id + " from database", sessionFactory);
    }
}
