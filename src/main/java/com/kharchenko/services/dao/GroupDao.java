package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.GroupEntity;
import com.kharchenko.entities.LessonEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class GroupDao extends HibernateCommonDao<GroupEntity>{

    private SessionFactory sessionFactory;

    public GroupDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<GroupEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<GroupEntity> cq = cb.createQuery(GroupEntity.class);
        Root<GroupEntity> rootEntry = cq.from(GroupEntity.class);
        CriteriaQuery<GroupEntity> all = cq.select(rootEntry);

        TypedQuery<GroupEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public GroupEntity create(GroupEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create group", sessionFactory);
    }

    public List<GroupEntity> createSeveral(List<GroupEntity> entities){
        for (GroupEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public GroupEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(GroupEntity.class, id),
                                       "Can't get group", sessionFactory);
    }

    @Override
    public GroupEntity update(GroupEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update group", sessionFactory);
    }

    @Override
    public boolean delete(GroupEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete group", sessionFactory);
    }

    public GroupEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            GroupEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete group with ID: " + id + " from database", sessionFactory);
    }
}
