package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.EducationYearEntity;
import com.kharchenko.entities.GroupCompositionEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class EducationYearDao extends HibernateCommonDao<EducationYearEntity>{

    private SessionFactory sessionFactory;

    public EducationYearDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<EducationYearEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EducationYearEntity> cq = cb.createQuery(EducationYearEntity.class);
        Root<EducationYearEntity> rootEntry = cq.from(EducationYearEntity.class);
        CriteriaQuery<EducationYearEntity> all = cq.select(rootEntry);

        TypedQuery<EducationYearEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public EducationYearEntity create(EducationYearEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create education year", sessionFactory);
    }

    public List<EducationYearEntity> createSeveral(List<EducationYearEntity> entities){
        for (EducationYearEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public EducationYearEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(EducationYearEntity.class, id),
                                       "Can't get education year", sessionFactory);
    }

    @Override
    public EducationYearEntity update(EducationYearEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update education year", sessionFactory);
    }

    @Override
    public boolean delete(EducationYearEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete education year", sessionFactory);
    }

    public EducationYearEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            EducationYearEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete education year with ID: " + id + " from database", sessionFactory);
    }

}
