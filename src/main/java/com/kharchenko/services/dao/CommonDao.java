package com.kharchenko.services.dao;

import java.util.List;

public interface CommonDao<T> {

    T getById(Long id);

    T create(T entity);

    List<T> createSeveral(List<T> entities);

    List<T> getAll();

    T update(T entity);

    boolean delete(T entity);

    T deleteById(Long id);
}
