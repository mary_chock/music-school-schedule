package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.CommunityEntity;
import com.kharchenko.entities.EducationYearEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class CommunityDao extends HibernateCommonDao<CommunityEntity>{

    private SessionFactory sessionFactory;

    public CommunityDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<CommunityEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<CommunityEntity> cq = cb.createQuery(CommunityEntity.class);
        Root<CommunityEntity> rootEntry = cq.from(CommunityEntity.class);
        CriteriaQuery<CommunityEntity> all = cq.select(rootEntry);

        TypedQuery<CommunityEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public CommunityEntity create(CommunityEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create community", sessionFactory);
    }

    public List<CommunityEntity> createSeveral(List<CommunityEntity> entities){
        for (CommunityEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public CommunityEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(CommunityEntity.class, id),
                                       "Can't get community", sessionFactory);
    }

    @Override
    public CommunityEntity update(CommunityEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update community", sessionFactory);
    }

    @Override
    public boolean delete(CommunityEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete community", sessionFactory);
    }

    public CommunityEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            CommunityEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete community with ID: " + id + " from database", sessionFactory);
    }
}
