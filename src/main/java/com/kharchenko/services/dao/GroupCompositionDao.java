package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.GroupCompositionEntity;
import com.kharchenko.entities.GroupEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class GroupCompositionDao extends HibernateCommonDao<GroupCompositionEntity>{

    private SessionFactory sessionFactory;

    public GroupCompositionDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<GroupCompositionEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<GroupCompositionEntity> cq = cb.createQuery(GroupCompositionEntity.class);
        Root<GroupCompositionEntity> rootEntry = cq.from(GroupCompositionEntity.class);
        CriteriaQuery<GroupCompositionEntity> all = cq.select(rootEntry);

        TypedQuery<GroupCompositionEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public GroupCompositionEntity create(GroupCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create group composition", sessionFactory);
    }

    public List<GroupCompositionEntity> createSeveral(List<GroupCompositionEntity> entities){
        for (GroupCompositionEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public GroupCompositionEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(GroupCompositionEntity.class, id),
                                       "Can't get group composition", sessionFactory);
    }

    @Override
    public GroupCompositionEntity update(GroupCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update group composition", sessionFactory);
    }

    @Override
    public boolean delete(GroupCompositionEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete group composition", sessionFactory);
    }

    public GroupCompositionEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            GroupCompositionEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete group composition with ID: " + id + " from database", sessionFactory);
    }

}
