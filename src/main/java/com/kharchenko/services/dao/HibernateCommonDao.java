package com.kharchenko.services.dao;

import com.kharchenko.exceptions.DaoException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.function.Function;

public abstract class HibernateCommonDao<T> implements CommonDao<T> {

    protected <N> N executeHibernateCommand(Function<Session, N> function, String error, SessionFactory sessionFactory) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();

                final N result = function.apply(session);

                session.getTransaction().commit();
                return result;
            } catch (Exception e) {
                sessionFactory.getCurrentSession().getTransaction().rollback();
                throw new DaoException(error, e);
            }
        }
    }
}
