package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.TeacherEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class TeacherDao extends HibernateCommonDao<TeacherEntity>{

    private SessionFactory sessionFactory;

    public TeacherDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<TeacherEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<TeacherEntity> cq = cb.createQuery(TeacherEntity.class);
        Root<TeacherEntity> rootEntry = cq.from(TeacherEntity.class);
        CriteriaQuery<TeacherEntity> all = cq.select(rootEntry);

        TypedQuery<TeacherEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public TeacherEntity create(TeacherEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create teacher", sessionFactory);
    }

    public List<TeacherEntity> createSeveral(List<TeacherEntity> entities){
        for (TeacherEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public TeacherEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(TeacherEntity.class, id),
                                       "Can't get teacher", sessionFactory);
    }

    @Override
    public TeacherEntity update(TeacherEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update teacher", sessionFactory);
    }

    @Override
    public boolean delete(TeacherEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete teacher", sessionFactory);
    }

    public TeacherEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            TeacherEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete teacher with ID: " + id + " from database", sessionFactory);
    }
}
