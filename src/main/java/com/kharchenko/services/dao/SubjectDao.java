package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.SubjectEntity;
import com.kharchenko.entities.SubjectTeacherEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class SubjectDao extends HibernateCommonDao<SubjectEntity>{

    private SessionFactory sessionFactory;

    public SubjectDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SubjectEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<SubjectEntity> cq = cb.createQuery(SubjectEntity.class);
        Root<SubjectEntity> rootEntry = cq.from(SubjectEntity.class);
        CriteriaQuery<SubjectEntity> all = cq.select(rootEntry);

        TypedQuery<SubjectEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public SubjectEntity create(SubjectEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create subject", sessionFactory);
    }

    public List<SubjectEntity> createSeveral(List<SubjectEntity> entities){
        for (SubjectEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public SubjectEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(SubjectEntity.class, id),
                                       "Can't get subject", sessionFactory);
    }

    @Override
    public SubjectEntity update(SubjectEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update subject", sessionFactory);
    }

    @Override
    public boolean delete(SubjectEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete subject", sessionFactory);
    }

    public SubjectEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            SubjectEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete subject with ID: " + id + " from database", sessionFactory);
    }
}
