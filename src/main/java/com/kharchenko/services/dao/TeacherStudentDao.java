package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.TeacherStudentEntity;
import com.kharchenko.entities.WorkTypeEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class TeacherStudentDao extends HibernateCommonDao<TeacherStudentEntity>{

    private SessionFactory sessionFactory;

    public TeacherStudentDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<TeacherStudentEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<TeacherStudentEntity> cq = cb.createQuery(TeacherStudentEntity.class);
        Root<TeacherStudentEntity> rootEntry = cq.from(TeacherStudentEntity.class);
        CriteriaQuery<TeacherStudentEntity> all = cq.select(rootEntry);

        TypedQuery<TeacherStudentEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public TeacherStudentEntity create(TeacherStudentEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create teacher-student composition", sessionFactory);
    }

    public List<TeacherStudentEntity> createSeveral(List<TeacherStudentEntity> entities){
        for (TeacherStudentEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public TeacherStudentEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(TeacherStudentEntity.class, id),
                                       "Can't get teacher-student composition", sessionFactory);
    }

    @Override
    public TeacherStudentEntity update(TeacherStudentEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update teacher-student composition", sessionFactory);
    }

    @Override
    public boolean delete(TeacherStudentEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete teacher-student composition", sessionFactory);
    }

    public TeacherStudentEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            TeacherStudentEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete teacher-student composition with ID: " + id + " from database", sessionFactory);
    }
}
