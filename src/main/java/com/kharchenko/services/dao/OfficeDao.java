package com.kharchenko.services.dao;

import com.kharchenko.entities.CommunityCompositionEntity;
import com.kharchenko.entities.OfficeEntity;
import com.kharchenko.entities.ScheduleEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class OfficeDao extends HibernateCommonDao<OfficeEntity>{

    private SessionFactory sessionFactory;

    public OfficeDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<OfficeEntity> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<OfficeEntity> cq = cb.createQuery(OfficeEntity.class);
        Root<OfficeEntity> rootEntry = cq.from(OfficeEntity.class);
        CriteriaQuery<OfficeEntity> all = cq.select(rootEntry);

        TypedQuery<OfficeEntity> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public OfficeEntity create(OfficeEntity entity) {
        return executeHibernateCommand(session -> {
            final Long storedEntityId = (Long) session.save(entity);
            entity.setId(storedEntityId);
            return entity;
        }, "Can't create office", sessionFactory);
    }

    public List<OfficeEntity> createSeveral(List<OfficeEntity> entities){
        for (OfficeEntity entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public OfficeEntity getById(Long id) {
        return executeHibernateCommand(session ->
                                               session.get(OfficeEntity.class, id),
                                       "Can't get office", sessionFactory);
    }

    @Override
    public OfficeEntity update(OfficeEntity entity) {
        return executeHibernateCommand(session -> {
            session.saveOrUpdate(entity);
            return entity;
        }, "Can't update office", sessionFactory);
    }

    @Override
    public boolean delete(OfficeEntity entity) {
        return executeHibernateCommand(session -> {
            session.delete(entity);
            return true;
        }, "Can't delete office", sessionFactory);
    }

    public OfficeEntity deleteById(Long id) {
        return executeHibernateCommand(session -> {
            OfficeEntity foundEntity = getById(id);
            session.delete(foundEntity);
            return foundEntity;
        }, "Can`t delete office with ID: " + id + " from database", sessionFactory);
    }
}
