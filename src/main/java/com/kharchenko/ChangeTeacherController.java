package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class ChangeTeacherController {

    @FXML
    public Button changeTeacherExitBtn;
    @FXML
    public Button changeTeacherSaveBtn;
    @FXML
    public Button changeTeacherAddSubjectBtn;
    @FXML
    public Button changeTeacherDeleteBtn;
    @FXML
    public Button changeTeacherDeleteSubjectBtn1;
    @FXML
    public Button changeTeacherDeleteSubjectBtn2;
    @FXML
    public Button changeTeacherDeleteSubjectBtn3;
    @FXML
    public Button changeTeacherDeleteSubjectBtn4;
    @FXML
    public Button changeTeacherDeleteSubjectBtn5;
    @FXML
    public Button changeTeacherDeleteSubjectBtn6;
    @FXML
    public Button changeTeacherDeleteSubjectBtn7;
    @FXML
    public TextField firstName;
    @FXML
    public TextField secondName;
    @FXML
    public TextField lastName;
    @FXML
    public TextField login;
    @FXML
    public TextField password;
    @FXML
    public ChoiceBox<OfficeEntity> officeNumberBox;
    @FXML
    public ChoiceBox<TeacherEntity> teacherBox;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox1;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox2;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox3;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox4;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox5;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox6;
    @FXML
    public ChoiceBox<WorkTypeEntity> workTypeBox7;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox1;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox2;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox3;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox4;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox5;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox6;
    @FXML
    public ChoiceBox<SubjectEntity> subjectBox7;

    private final Resource fxml4;
    private final ApplicationContext applicationContext;
    private final WorkTypeDao workTypeDao;
    private final SubjectDao subjectDao;
    private final OfficeDao officeDao;
    private final TeacherDao teacherDao;
    private final SubjectTeacherDao subjectTeacherDao;
    private final ScheduleDao scheduleDao;
    private final CommunityDao communityDao;
    private final StudentSubjectDao studentSubjectDao;
    private final CommunityCompositionDao communityCompositionDao;
    private final TeacherStudentDao teacherStudentDao;
    private List<SubjectEntity> subjectEntities;
    private int count;
    private List<ChoiceBox<WorkTypeEntity>> works;
    private List<ChoiceBox<SubjectEntity>> subjects;

    public ChangeTeacherController(@Value("classpath:/scenes/main-menu-db.fxml") Resource fxml4,
                                   ApplicationContext applicationContext,
                                   WorkTypeDao workTypeDao,
                                   SubjectDao subjectDao,
                                   OfficeDao officeDao,
                                   TeacherDao teacherDao,
                                   SubjectTeacherDao subjectTeacherDao,
                                   ScheduleDao scheduleDao,
                                   CommunityDao communityDao,
                                   CommunityCompositionDao communityCompositionDao,
                                   TeacherStudentDao teacherStudentDao,
                                   StudentSubjectDao studentSubjectDao) {
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
        this.workTypeDao = workTypeDao;
        this.subjectDao = subjectDao;
        this.officeDao = officeDao;
        this.teacherDao = teacherDao;
        this.subjectTeacherDao = subjectTeacherDao;
        this.scheduleDao = scheduleDao;
        this.studentSubjectDao = studentSubjectDao;
        this.communityDao = communityDao;
        this.communityCompositionDao = communityCompositionDao;
        this.teacherStudentDao = teacherStudentDao;
    }

    @FXML
    public void initialize() {
        count = 1;
        subjectEntities = subjectDao.getAll();
        setInitialVisibility();
        initBoxesLists();
        this.changeTeacherExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                changeScene(this.fxml4, changeTeacherExitBtn);
        });
        this.changeTeacherAddSubjectBtn.setOnAction(actionEvent -> addSubject());
        setWorkItems(workTypeBox);
        setOfficeItems(officeNumberBox);
        setTeacherItems();
        ChangeListener<TeacherEntity> changeListener = (observableValue, teacherEntity, t1) -> {
            if (t1 != null) {
                setTeacherInfo();
            }
        };
        teacherBox.getSelectionModel().selectedItemProperty().addListener(changeListener);
        this.subjectBox.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox, workTypeBox));
        this.subjectBox1.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox1, workTypeBox1));
        this.subjectBox2.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox2, workTypeBox2));
        this.subjectBox3.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox3, workTypeBox3));
        this.subjectBox4.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox4, workTypeBox4));
        this.subjectBox5.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox5, workTypeBox5));
        this.subjectBox6.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox6, workTypeBox6));
        this.subjectBox7.setOnMouseClicked(mouseEvent -> checkWorkForSubject(subjectBox7, workTypeBox7));
        this.changeTeacherDeleteSubjectBtn1
                .setOnAction(actionEvent -> clear(workTypeBox1, subjectBox1));
        this.changeTeacherDeleteSubjectBtn2
                .setOnAction(actionEvent -> clear(workTypeBox2, subjectBox2));
        this.changeTeacherDeleteSubjectBtn3
                .setOnAction(actionEvent -> clear(workTypeBox3, subjectBox3));
        this.changeTeacherDeleteSubjectBtn4
                .setOnAction(actionEvent -> clear(workTypeBox4, subjectBox4));
        this.changeTeacherDeleteSubjectBtn5
                .setOnAction(actionEvent -> clear(workTypeBox5, subjectBox5));
        this.changeTeacherDeleteSubjectBtn6
                .setOnAction(actionEvent -> clear(workTypeBox6, subjectBox6));
        this.changeTeacherDeleteSubjectBtn7
                .setOnAction(actionEvent -> clear(workTypeBox7, subjectBox7));
        this.changeTeacherSaveBtn.setOnAction(actionEvent -> saveTeacher());
        this.changeTeacherDeleteBtn.setOnAction(actionEvent -> deleteTeacher());
    }

    private void deleteTeacher() {
        if (teacherBox.getValue() != null)
            if (showDeleteConfirmation()) {
                for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
                    if (scheduleEntity.getScheduleTeacherEntity().getId().equals(teacherBox.getValue().getId()))
                        scheduleDao.deleteById(scheduleEntity.getId());
                }
                for (CommunityCompositionEntity communityCompositionEntity : communityCompositionDao.getAll()) {
                    if (communityCompositionEntity.getCommunityCommunityCompositionEntity().getTeacherEntity().getId()
                                                  .equals(teacherBox.getValue().getId()))
                        communityCompositionDao.deleteById(communityCompositionEntity.getId());
                }
//                for (CommunityEntity communityEntity : communityDao.getAll()) {
//                    if (communityEntity.getTeacherEntity().getId().equals(teacherBox.getValue().getId()))
//                        communityDao.deleteById(communityEntity.getId());
//                }
                for (TeacherStudentEntity teacherStudentEntity : teacherStudentDao.getAll()) {
                    if (teacherStudentEntity.getTeacherTeacherStudentEntity().getId()
                                            .equals(teacherBox.getValue().getId()))
                        teacherStudentDao.deleteById(teacherStudentEntity.getId());
                }
                for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                    if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId()
                                            .equals(teacherBox.getValue().getId())) {
                        for (StudentSubjectEntity studentSubjectEntity : studentSubjectDao.getAll()) {
                            if(studentSubjectEntity.getTeacherSubjectStudentSubjectEntity().getId().equals(subjectTeacherEntity.getId()))
                                studentSubjectDao.deleteById(studentSubjectEntity.getId());
                        }
                        subjectTeacherDao.deleteById(subjectTeacherEntity.getId());
                    }
                }
                teacherDao.deleteById(teacherBox.getValue().getId());
                changeScene(fxml4, changeTeacherExitBtn);
            }
    }

    private boolean showSaveConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Сохранение преподавателя");
        alert.setHeaderText("Внести изменения для преподавателя '" + teacherBox.getValue().toString() + "'?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Выйти без сохранения изменений?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private boolean showDeleteConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Удаление преподавателя");
        alert.setHeaderText("Удалить данные о преподавателе '" + teacherBox.getValue().toString() + "'?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }

    private void setTeacherItems() {
        ObservableList<TeacherEntity> teachers = FXCollections.observableArrayList();
        teachers.addAll(teacherDao.getAll());
        teacherBox.setItems(teachers);
    }

    private void setTeacherInfo() {
        if (teacherBox.getValue() != null) {
            TeacherEntity teacherEntity = teacherBox.getValue();
            firstName.setText(teacherEntity.getFirstName());
            secondName.setText(teacherEntity.getSecondName());
            lastName.setText(teacherEntity.getLastName());
            login.setText(teacherEntity.getLogin());
            password.setText(teacherEntity.getPassword());
            ObservableList<OfficeEntity> offices = setOfficeItems(officeNumberBox);
            for (OfficeEntity office : offices) {
                if (office.getId().equals(teacherEntity.getOfficeEntity().getId()))
                    officeNumberBox.setValue(office);
            }
            setTeacherSubjects();
        }
    }

    private void setTeacherSubjects() {
        workTypeBox.setValue(null);
        subjectBox.setValue(null);
        setInitialVisibility();
        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
            if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId().equals(teacherBox.getValue().getId())) {
                addSubject();
                for (int i = 0; i < 8; i++) {
                    if (!checkSubject(works.get(i), subjects.get(i))) {
                        ObservableList<WorkTypeEntity> workTypes = setWorkItems(works.get(i));
                        for (WorkTypeEntity workType : workTypes) {
                            if (workType.getId().equals(subjectTeacherEntity.getSubjectTeacherSubjectEntity()
                                                                            .getWorkTypeEntity().getId()))
                                works.get(i).setValue(workType);
                        }
                        ObservableList<SubjectEntity> subjectsList = setSubjectItems(subjects.get(i), works.get(i));
                        for (SubjectEntity subjectEntity : subjectsList) {
                            if (subjectEntity.getId()
                                             .equals(subjectTeacherEntity.getSubjectTeacherSubjectEntity().getId()))
                                subjects.get(i).setValue(subjectEntity);
                        }
                        break;
                    }
                }
            }
        }
    }

    private void initBoxesLists() {
        works = new ArrayList<>();
        subjects = new ArrayList<>();
        works.add(workTypeBox);
        works.add(workTypeBox1);
        works.add(workTypeBox2);
        works.add(workTypeBox3);
        works.add(workTypeBox4);
        works.add(workTypeBox5);
        works.add(workTypeBox6);
        works.add(workTypeBox7);
        subjects.add(subjectBox);
        subjects.add(subjectBox1);
        subjects.add(subjectBox2);
        subjects.add(subjectBox3);
        subjects.add(subjectBox4);
        subjects.add(subjectBox5);
        subjects.add(subjectBox6);
        subjects.add(subjectBox7);
    }

    private void saveTeacher() {

        if (checkInput() && officeNumberBox.getValue() != null)
            if (showSaveConfirmation()) {
                if (checkSubject(workTypeBox, subjectBox)) {
                    if (!hasSame()) {
                        TeacherEntity teacherEntity = new TeacherEntity();
                        teacherEntity.setId(teacherBox.getValue().getId());
                        teacherEntity.setFirstName(firstName.getText());
                        teacherEntity.setSecondName(secondName.getText());
                        teacherEntity.setLastName(lastName.getText());
                        teacherEntity.setOfficeEntity(officeNumberBox.getValue());
                        teacherEntity.setLogin(login.getText());
                        teacherEntity.setPassword(password.getText());

                        for (SubjectTeacherEntity subjectTeacherEntity : subjectTeacherDao.getAll()) {
                            if (subjectTeacherEntity.getTeacherTeacherSubjectEntity().getId()
                                                    .equals(teacherBox.getValue().getId()))
                                subjectTeacherDao.deleteById(subjectTeacherEntity.getId());
                        }
                        teacherDao.update(teacherEntity);

                        for (int i = 0; i < 8; i++) {
                            if (checkSubject(works.get(i), subjects.get(i))) {
                                SubjectTeacherEntity subjectTeacherEntity = new SubjectTeacherEntity();
                                subjectTeacherEntity.setTeacherTeacherSubjectEntity(teacherEntity);
                                subjectTeacherEntity.setSubjectTeacherSubjectEntity(subjects.get(i).getValue());
                                subjectTeacherDao.update(subjectTeacherEntity);
                            }
                        }

                        changeScene(fxml4, changeTeacherExitBtn);
                    }
                }
            }
    }

    private boolean hasSame() {
        Set<String> strings = new TreeSet<>();
        int counter = 0;
        for (ChoiceBox<SubjectEntity> subject : subjects) {
            if (subject.getValue() != null) {
                strings.add(subject.getValue().getName());
                counter++;
            }
        }
        if (strings.size() < counter) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Вы выбрали одинаковые дисциплины");
            alert.showAndWait();
            return true;
        }

        return false;
    }

    private boolean checkSubject(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject) {
        return workType.getValue() != null && subject.getValue() != null;
    }

    private boolean checkInput() {
        String nameRegEx = "^[А-Я].+[а-я]$";
        String loginRegEx = "^[a-zA-Z]{3,}$";
        String passRegEx = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        if (!firstName.getText().isEmpty()
                && !secondName.getText().isEmpty()
                && !lastName.getText().isEmpty()
                && !login.getText().isEmpty()
                && !password.getText().isEmpty()) {

            if (!firstName.getText().matches(nameRegEx)) {
                setAlert("Имя должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!secondName.getText().matches(nameRegEx)) {
                setAlert("Отчество должно начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!lastName.getText().matches(nameRegEx)) {
                setAlert("Фамилия должна начинаться с заглавной буквы, только русский язык");
                return false;
            }
            if (!login.getText().matches(loginRegEx)) {
                setAlert("Логин должен состоять минимум из 3 английских символов");
                return false;
            }
            if (!password.getText().matches(passRegEx)) {
                setAlert("Пароль должен содержать цифры, заглавные и строчные английские символы. длина не меньше 8");
                return false;
            }

        }
        return true;
    }

    private void setAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Неправильный ввод");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void checkWorkForSubject(ChoiceBox<SubjectEntity> subject, ChoiceBox<WorkTypeEntity> workType) {
        if (workType.getValue() != null)
            setSubjectItems(subject, workType);
    }

    private ObservableList<SubjectEntity> setSubjectItems(ChoiceBox<SubjectEntity> subject,
                                                          ChoiceBox<WorkTypeEntity> workType) {

        ObservableList<SubjectEntity> subjects = FXCollections.observableArrayList();
        for (SubjectEntity subjectEntity : subjectEntities) {
            if (!subjectEntity.getName().equals("Коллектив"))
                if (workType.getValue().getName()
                            .equals(String.valueOf(subjectEntity.getWorkTypeEntity().getName())))
                    subjects.add(subjectEntity);
        }
        subject.setItems(subjects);
        return subjects;
    }

    private ObservableList<OfficeEntity> setOfficeItems(ChoiceBox<OfficeEntity> office) {
        ObservableList<OfficeEntity> offices = FXCollections.observableArrayList();
        offices.addAll(officeDao.getAll());
        office.setItems(offices);
        return offices;
    }

    private ObservableList<WorkTypeEntity> setWorkItems(ChoiceBox<WorkTypeEntity> workType) {
        ObservableList<WorkTypeEntity> workTypes = FXCollections.observableArrayList();
        for (WorkTypeEntity workTypeEntity : workTypeDao.getAll()) {
            if (!workTypeEntity.getName().equals("Коллективная"))
                workTypes.add(workTypeEntity);

        }
        workType.setItems(workTypes);
        return workTypes;
    }

    private void addSubject() {
        switch (count) {
            case 1:
                if (workTypeBox.getValue() != null && subjectBox.getValue() != null) {
                    setVisible(workTypeBox1, subjectBox1, changeTeacherDeleteSubjectBtn1);
                    setWorkItems(workTypeBox1);
                }
                break;
            case 2:
                if (workTypeBox1.getValue() != null && subjectBox1.getValue() != null) {
                    setVisible(workTypeBox2, subjectBox2, changeTeacherDeleteSubjectBtn2);
                    setWorkItems(workTypeBox2);
                }
                break;
            case 3:
                if (workTypeBox2.getValue() != null && subjectBox2.getValue() != null) {
                    setVisible(workTypeBox3, subjectBox3, changeTeacherDeleteSubjectBtn3);
                    setWorkItems(workTypeBox3);
                }
                break;
            case 4:
                if (workTypeBox3.getValue() != null && subjectBox3.getValue() != null) {
                    setVisible(workTypeBox4, subjectBox4, changeTeacherDeleteSubjectBtn4);
                    setWorkItems(workTypeBox4);
                }
                break;
            case 5:
                if (workTypeBox4.getValue() != null && subjectBox4.getValue() != null) {
                    setVisible(workTypeBox5, subjectBox5, changeTeacherDeleteSubjectBtn5);
                    setWorkItems(workTypeBox5);
                }
                break;
            case 6:
                if (workTypeBox5.getValue() != null && subjectBox5.getValue() != null) {
                    setVisible(workTypeBox6, subjectBox6, changeTeacherDeleteSubjectBtn6);
                    setWorkItems(workTypeBox6);
                }
                break;
            case 7:
                if (workTypeBox6.getValue() != null && subjectBox6.getValue() != null) {
                    setVisible(workTypeBox7, subjectBox7, changeTeacherDeleteSubjectBtn7);
                    setWorkItems(workTypeBox7);
                }
                break;
        }
    }

    private void setVisible(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject, Button delete) {
        workType.setVisible(true);
        subject.setVisible(true);
        delete.setVisible(true);
        count++;
    }

    private void clear(ChoiceBox<WorkTypeEntity> workType, ChoiceBox<SubjectEntity> subject) {
        workType.setValue(null);
        subject.setValue(null);
        workType.setVisible(false);
        subject.setVisible(false);
        workType.setVisible(true);
        subject.setVisible(true);
    }

    private void changeScene(Resource fxml, Button button) {
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setInitialVisibility() {
        this.changeTeacherDeleteSubjectBtn1.setVisible(false);
        this.changeTeacherDeleteSubjectBtn2.setVisible(false);
        this.changeTeacherDeleteSubjectBtn3.setVisible(false);
        this.changeTeacherDeleteSubjectBtn4.setVisible(false);
        this.changeTeacherDeleteSubjectBtn5.setVisible(false);
        this.changeTeacherDeleteSubjectBtn6.setVisible(false);
        this.changeTeacherDeleteSubjectBtn7.setVisible(false);
        this.workTypeBox1.setVisible(false);
        this.workTypeBox2.setVisible(false);
        this.workTypeBox3.setVisible(false);
        this.workTypeBox4.setVisible(false);
        this.workTypeBox5.setVisible(false);
        this.workTypeBox6.setVisible(false);
        this.workTypeBox7.setVisible(false);
        this.subjectBox1.setVisible(false);
        this.subjectBox2.setVisible(false);
        this.subjectBox3.setVisible(false);
        this.subjectBox4.setVisible(false);
        this.subjectBox5.setVisible(false);
        this.subjectBox6.setVisible(false);
        this.subjectBox7.setVisible(false);
        count = 1;
    }

}
