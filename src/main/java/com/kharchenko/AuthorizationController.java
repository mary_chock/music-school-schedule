package com.kharchenko;

import com.kharchenko.JavaFxApplication;
import com.kharchenko.entities.TeacherEntity;
import com.kharchenko.services.dao.TeacherDao;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@Component
public class AuthorizationController {

    @FXML
    public TextField loginField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public Button loginBtn;
    @FXML
    public Button studentBtn;
    @FXML
    public Button aboutProgramBtn;
//todo make aboutBtn onClick()
    private TeacherDao teacherDao;
    private final Resource fxml1;
    private final Resource fxml2;
    private final Resource fxml3;
    private final ApplicationContext applicationContext;

    public static TeacherEntity loggedTeacher;

    public AuthorizationController(TeacherDao teacherDao,
                                   @Value("classpath:/scenes/main-menu-db.fxml") Resource resource,
                                   @Value("classpath:/scenes/student-schedule.fxml") Resource resource2,
                                   @Value("classpath:/scenes/teacher-schedule.fxml") Resource resource1,
                                   ApplicationContext ac) {
        this.teacherDao = teacherDao;
        this.fxml1 = resource;
        this.fxml2 = resource1;
        this.fxml3 = resource2;
        this.applicationContext = ac;
    }

    public static TeacherEntity getLoggedTeacher() {
        return loggedTeacher;
    }

    @FXML
    public void initialize() {
        this.studentBtn.setOnAction(actionEvent -> {
            try {
                Stage stage = (Stage) loginBtn.getScene().getWindow();
                URL url = this.fxml3.getURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                fxmlLoader.setControllerFactory(applicationContext::getBean);
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        this.loginBtn.setOnAction(actionEvent -> {
            List<TeacherEntity> teacherEntities = teacherDao.getAll();
            TeacherEntity teacher = null;
            if (!loginField.getText().isEmpty() && !passwordField.getText().isEmpty()) {
                String log = loginField.getText();
                String pass = passwordField.getText();
                String tlog = "";
                String tpas = "";
                for (TeacherEntity teach : teacherEntities) {
                    tlog = teach.getLogin();
                    tpas = teach.getPassword();
                    if (log.equals(String.valueOf(tlog)) && pass.equals(String.valueOf(tpas))) {
                        teacher = teach;
                        break;
                    }
                }
                if (teacher == null) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Ошибка входа");
                    alert.setHeaderText(null);
                    alert.setContentText("Вы ввели не правильный логин или пароль!");
                    alert.showAndWait();
                } else if ("admin".equals(String.valueOf(teacher.getLogin()))) {
                    try {
                        Stage stage = (Stage) loginBtn.getScene().getWindow();
                        URL url = this.fxml1.getURL();
                        FXMLLoader fxmlLoader = new FXMLLoader(url);
                        fxmlLoader.setControllerFactory(applicationContext::getBean);
                        Parent root = fxmlLoader.load();
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                } else {
                    try {
                        for (TeacherEntity teacherEntity : teacherDao.getAll()) {
                            if(teacherEntity.getLogin().equals(String.valueOf(loginField.getText())))
                                loggedTeacher = teacherEntity;
                        }
                        Stage stage = (Stage) loginBtn.getScene().getWindow();
                        URL url = this.fxml2.getURL();
                        FXMLLoader fxmlLoader = new FXMLLoader(url);
                        fxmlLoader.setControllerFactory(applicationContext::getBean);
                        Parent root = fxmlLoader.load();
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

            }
        });
    }


}
