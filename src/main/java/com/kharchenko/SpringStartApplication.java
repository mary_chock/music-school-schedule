package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.TestService;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Calendar;

@SpringBootApplication
public class SpringStartApplication {
    public static void main(String[] args) {

        //SpringApplication.run(SpringStartApplication.class, args);
        Application.launch(JavaFxApplication.class, args);
//        EducationYearEntity educationYearEntity = new EducationYearEntity();
//        educationYearEntity.setYearNumber(1);
//
//        OfficeEntity officeEntity = new OfficeEntity();
//        officeEntity.setOfficeNumber(228);
//
//        WorkTypeEntity workTypeEntity = new WorkTypeEntity();
//        workTypeEntity.setName("Специальность");
//
//        StudentEntity studentEntity = new StudentEntity();
//        studentEntity.setFirstName("Мария");
//        studentEntity.setSecondName("Михайловна");
//        studentEntity.setLastName("Харченко");
//        studentEntity.setStudentEducationYearEntity(educationYearEntity);
//
//        TeacherEntity teacherEntity = new TeacherEntity();
//        teacherEntity.setFirstName("Ольга");
//        teacherEntity.setSecondName("Михайловна");
//        teacherEntity.setLastName("Коротко");
//        teacherEntity.setLogin("admin");
//        teacherEntity.setPassword("admin");
//        teacherEntity.setOfficeEntity(officeEntity);
//
//        SubjectEntity subjectEntity = new SubjectEntity();
//        subjectEntity.setName("Литература");
//        subjectEntity.setWorkTypeEntity(workTypeEntity);
//
//        LessonEntity lessonEntity = new LessonEntity();
//        lessonEntity.setLessonNumber(1);
//        lessonEntity.setStartTime("9:00");
//        lessonEntity.setEndTime("9:45");
//
//        TeacherStudentEntity teacherStudentEntity = new TeacherStudentEntity();
//        teacherStudentEntity.setTeacherTeacherStudentEntity(teacherEntity);
//        teacherStudentEntity.setStudentTeacherStudentEntity(studentEntity);
//
//        CommunityEntity communityEntity = new CommunityEntity();
//        communityEntity.setName("Солнышко");
//        communityEntity.setTeacherEntity(teacherEntity);
//
//        CommunityCompositionEntity communityCompositionEntity = new CommunityCompositionEntity();
//        communityCompositionEntity.setCommunityCommunityCompositionEntity(communityEntity);
//        communityCompositionEntity.setStudentCommunityCompositionEntity(studentEntity);
//
//        GroupEntity groupEntity = new GroupEntity();
//        groupEntity.setGroupEducationYearEntity(educationYearEntity);
//
//        GroupCompositionEntity groupCompositionEntity = new GroupCompositionEntity();
//        groupCompositionEntity.setGroupGroupCompositionEntity(groupEntity);
//        groupCompositionEntity.setStudentGroupCompositionEntity(studentEntity);
//
//        SubjectTeacherEntity subjectTeacherEntity = new SubjectTeacherEntity();
//        subjectTeacherEntity.setSubjectTeacherSubjectEntity(subjectEntity);
//        subjectTeacherEntity.setTeacherTeacherSubjectEntity(teacherEntity);
//
//        ScheduleEntity scheduleEntity = new ScheduleEntity();
//        scheduleEntity.setStartDate(Calendar.getInstance().getTime());
//        scheduleEntity.setEndDate(Calendar.getInstance().getTime());
//        scheduleEntity.setWeekDay("Понедельник");
//        scheduleEntity.setScheduleTeacherSubjectEntity(subjectTeacherEntity);
//        scheduleEntity.setScheduleTeacherEntity(teacherEntity);
//        scheduleEntity.setScheduleTeacherStudentEntity(teacherStudentEntity);
//        scheduleEntity.setScheduleGroupCompositionEntity(groupCompositionEntity);
//        scheduleEntity.setScheduleCommunityCompositionEntity(communityCompositionEntity);
//        scheduleEntity.setScheduleLessonEntity(lessonEntity);
//
//        applicationContext.getBean(TestService.class).startTestService(educationYearEntity, officeEntity, workTypeEntity,
//                                                            studentEntity, teacherEntity, subjectEntity,
//                                                            lessonEntity, teacherStudentEntity, communityEntity,
//                                                            communityCompositionEntity, groupEntity, groupCompositionEntity,
//                                                            subjectTeacherEntity, scheduleEntity);
//        applicationContext.getBean(TestService.class).getAllTeachers();

    }
}
