package com.kharchenko;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class ChooseToAddController {

    @FXML
    public Button menuAddStudentBtn;
    @FXML
    public Button menuAddTeacherBtn;
    @FXML
    public Button menuAddCommBtn;
    @FXML
    public Button menuAddBackBtn;

    private final Resource fxml1;
    private final Resource fxml2;
    private final Resource fxml3;
    private final Resource fxml4;
    private final ApplicationContext applicationContext;

    public ChooseToAddController(@Value("classpath:/scenes/add-student.fxml")Resource fxml1,
                                 @Value("classpath:/scenes/add-teacher.fxml")Resource fxml2,
                                 @Value("classpath:/scenes/add-community.fxml")Resource fxml3,
                                 @Value("classpath:/scenes/main-menu-db.fxml")Resource fxml4,
                                 ApplicationContext applicationContext) {
        this.fxml1 = fxml1;
        this.fxml2 = fxml2;
        this.fxml3 = fxml3;
        this.fxml4 = fxml4;
        this.applicationContext = applicationContext;
    }

    @FXML
    public void initialize(){
        this.menuAddStudentBtn.setOnAction(actionEvent ->{
            changeScene(this.fxml1, menuAddStudentBtn);
        });

        this.menuAddTeacherBtn.setOnAction(actionEvent ->{
            changeScene(this.fxml2, menuAddTeacherBtn);
        });

        this.menuAddCommBtn.setOnAction(actionEvent ->{
            changeScene(this.fxml3, menuAddCommBtn);
        });

        this.menuAddBackBtn.setOnAction(actionEvent ->{
            changeScene(this.fxml4, menuAddBackBtn);
        });
    }

    private void changeScene(Resource fxml, Button button){
        try {
            Stage stage = (Stage) button.getScene().getWindow();
            URL url = fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
