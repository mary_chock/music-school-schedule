package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.dao.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class StudentScheduleController {

    @FXML
    public Button scheduleExitBtn;
    @FXML
    public ChoiceBox<EducationYearEntity> classBox;
    @FXML
    public ChoiceBox<StudentEntity> studentBox;
    @FXML
    public TableView<ScheduleEntity> table;
    @FXML
    public ListView<String> list;
    @FXML
    public ListView<String> list1;
    @FXML
    public ListView<String> list2;
    @FXML
    public ListView<String> list3;
    @FXML
    public ListView<String> list4;
    @FXML
    public ListView<String> list5;
    @FXML
    public ListView<String> list6;
    @FXML
    public Label week1Label;
    @FXML
    public Label week2Label;
    @FXML
    public Label week3Label;
    @FXML
    public Label week4Label;
    @FXML
    public Label week5Label;
    @FXML
    public Label week6Label;

    private final Resource fxml1;
    private final Resource fxml2;
    private final ApplicationContext applicationContext;
    private final ScheduleDao scheduleDao;
    private final LessonDao lessonDao;
    private final StudentDao studentDao;
    private final EducationYearDao educationYearDao;
    private final GroupCompositionDao groupCompositionDao;
    private final CommunityCompositionDao communityCompositionDao;

    public StudentScheduleController(@Value("classpath:/scenes/make-schedule-for-group-community.fxml") Resource fxml1,
                                     @Value("classpath:/scenes/delete-schedule.fxml") Resource fxml2,
                                     ApplicationContext applicationContext,
                                     ScheduleDao scheduleDao,
                                     LessonDao lessonDao,
                                     StudentDao studentDao,
                                     GroupCompositionDao groupCompositionDao,
                                     CommunityCompositionDao communityCompositionDao,
                                     EducationYearDao educationYearDao) {

        this.fxml1 = fxml1;
        this.fxml2 = fxml2;
        this.applicationContext = applicationContext;
        this.scheduleDao = scheduleDao;
        this.lessonDao = lessonDao;
        this.studentDao = studentDao;
        this.groupCompositionDao = groupCompositionDao;
        this.communityCompositionDao = communityCompositionDao;
        this.educationYearDao = educationYearDao;
    }

    @FXML
    public void initialize() {
        setClassItems();
        scheduleExitBtn.setOnAction(actionEvent -> {
            if (showExitConfirmation())
                Platform.exit();
        });
        ChangeListener<StudentEntity> changeListener = (observableValue, studentEntity, s1) -> {
            if (s1 != null) {
                setScheduleItems();
            }
        };
        studentBox.getSelectionModel().selectedItemProperty().addListener(changeListener);
        ChangeListener<EducationYearEntity> changeListener1 = (observableValue, studentEntity, s1) -> {
            if (s1 != null) {
                setStudentItems();
            }
        };
        classBox.getSelectionModel().selectedItemProperty().addListener(changeListener1);
    }

    private void setClassItems(){
        ObservableList<EducationYearEntity> years = FXCollections.observableArrayList();
        years.addAll(educationYearDao.getAll());
        classBox.setItems(years);
    }

    private void setStudentItems(){
        ObservableList<StudentEntity> students = FXCollections.observableArrayList();
        for (StudentEntity studentEntity : studentDao.getAll()) {
            if(studentEntity.getStudentEducationYearEntity().getId().equals(classBox.getValue().getId()))
                students.add(studentEntity);
        }
        studentBox.setItems(students);
    }

    private void setScheduleItems() {
        List<ScheduleEntity> schedules = new ArrayList<>();
        for (ScheduleEntity scheduleEntity : scheduleDao.getAll()) {
            if(scheduleEntity.getScheduleTeacherSubjectEntity().getSubjectTeacherSubjectEntity().getWorkTypeEntity().getId().equals(14L))
            {
                if (scheduleEntity.getScheduleStudentSubjectEntity().getStudentStudentSubjectEntity().getId()
                                  .equals(studentBox.getValue().getId())) {
                    schedules.add(scheduleEntity);
                }
            }else if(scheduleEntity.getScheduleTeacherSubjectEntity().getSubjectTeacherSubjectEntity().getWorkTypeEntity().getId().equals(15L))
            {
                for (GroupCompositionEntity groupCompositionEntity : groupCompositionDao.getAll()) {
                    if(groupCompositionEntity.getGroupGroupCompositionEntity().getId().equals(scheduleEntity.getScheduleGroupEntity().getId()) && groupCompositionEntity.getStudentGroupCompositionEntity().getId().equals(studentBox.getValue().getId())){
                        schedules.add(scheduleEntity);
                    }
                }
            }else {
                for (CommunityCompositionEntity groupCompositionEntity : communityCompositionDao.getAll()) {
                    if(groupCompositionEntity.getCommunityCommunityCompositionEntity().getId().equals(scheduleEntity.getScheduleCommunityEntity().getId()) && groupCompositionEntity.getStudentCommunityCompositionEntity().getId().equals(studentBox.getValue().getId())){
                        schedules.add(scheduleEntity);
                    }
                }
            }

        }
        ObservableList<String> lessons = FXCollections.observableArrayList();
        for (LessonEntity lessonEntity : lessonDao.getAll()) {
            lessons.add(lessonEntity.getLessonNumber() + ".\n" + lessonEntity.getStartTime() + "\n" + lessonEntity
                    .getEndTime()+"\n ");
        }
        ObservableList<String> week1 = FXCollections.observableArrayList();
        ObservableList<String> week2 = FXCollections.observableArrayList();
        ObservableList<String> week3 = FXCollections.observableArrayList();
        ObservableList<String> week4 = FXCollections.observableArrayList();
        ObservableList<String> week5 = FXCollections.observableArrayList();
        ObservableList<String> week6 = FXCollections.observableArrayList();
        String[] w1 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w2 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w3 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w4 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w5 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        String[] w6 = new String[]{"\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n","\n\n\n\n"};
        for (ScheduleEntity schedule : schedules) {
            if (schedule.getWeekDay().equals(String.valueOf(week1Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w1[index]=schedule.getStudentInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week2Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w2[index]=schedule.getStudentInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week3Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w3[index]=schedule.getStudentInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week4Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w4[index]=schedule.getStudentInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week5Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w5[index]=schedule.getStudentInfo();
            } else if (schedule.getWeekDay().equals(String.valueOf(week6Label.getText()))) {
                int index = schedule.getScheduleLessonEntity().getLessonNumber()-1;
                w6[index]=schedule.getStudentInfo();
            }
        }
        week1.addAll(w1);
        week2.addAll(w2);
        week3.addAll(w3);
        week4.addAll(w4);
        week5.addAll(w5);
        week6.addAll(w6);
        list.setItems(lessons);
        list1.setItems(week1);
        list2.setItems(week2);
        list3.setItems(week3);
        list4.setItems(week4);
        list5.setItems(week5);
        list6.setItems(week6);
    }

    private boolean showExitConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Выход");
        alert.setHeaderText("Закрыть приложение?");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            return true;
        } else if (option.get() == ButtonType.CANCEL) {
            return false;
        }
        return false;
    }
}
