package com.kharchenko;

import com.kharchenko.entities.*;
import com.kharchenko.services.TestService;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class StageListener implements ApplicationListener<StageReadyEvent> {

    private final String applicationTitle;
    private final Resource fxml;
    private final ApplicationContext applicationContext;

    public StageListener(@Value("${spring.application.ui.title}") String applicationTitle,
                         @Value("classpath:/scenes/authorization.fxml") Resource resource,
                         ApplicationContext ac) {
        this.applicationTitle = applicationTitle;
        this.fxml = resource;
        this.applicationContext = ac;
    }

    @Override
    public void onApplicationEvent(StageReadyEvent stageReadyEvent) {
        try {
            Stage stage = stageReadyEvent.getStage();
            URL url = this.fxml.getURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle(this.applicationTitle);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void executeHiber(){
        List<EducationYearEntity> yearEntities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            EducationYearEntity educationYearEntity = new EducationYearEntity();
            educationYearEntity.setYearNumber(i);
            yearEntities.add(educationYearEntity);
        }
        List<OfficeEntity> officeEntities = new ArrayList<>();
        for (int i = 1; i < 41; i++) {
            OfficeEntity officeEntity = new OfficeEntity();
            officeEntity.setOfficeNumber(i);
            officeEntities.add(officeEntity);
        }
        List<WorkTypeEntity> workTypeEntities = new ArrayList<>();
        WorkTypeEntity workInd = new WorkTypeEntity();
        workInd.setName("Индивидуальная");
        WorkTypeEntity workGroup = new WorkTypeEntity();
        workGroup.setName("Групповая");
        WorkTypeEntity workComm = new WorkTypeEntity();
        workComm.setName("Коллективная");
        workTypeEntities.add(workInd);
        workTypeEntities.add(workGroup);
        workTypeEntities.add(workComm);
        List<SubjectEntity> subjectEntities = new ArrayList<>();
        SubjectEntity subject = new SubjectEntity();
        subject.setName("Cольфеджио");
        subject.setWorkTypeEntity(workGroup);
        SubjectEntity subject1 = new SubjectEntity();
        subject1.setName("Муз.литература");
        subject1.setWorkTypeEntity(workGroup);
        SubjectEntity subject2 = new SubjectEntity();
        subject2.setName("Хор");
        subject2.setWorkTypeEntity(workComm);
        SubjectEntity subject3 = new SubjectEntity();
        subject3.setName("Ансамбль");
        subject3.setWorkTypeEntity(workComm);
        SubjectEntity subject4 = new SubjectEntity();
        subject4.setName("Группа");
        subject4.setWorkTypeEntity(workComm);
        SubjectEntity subject5 = new SubjectEntity();
        subject5.setName("Спец.фоно");
        subject5.setWorkTypeEntity(workInd);
        SubjectEntity subject6 = new SubjectEntity();
        subject6.setName("Cпец.сопилка");
        subject6.setWorkTypeEntity(workInd);
        SubjectEntity subject7 = new SubjectEntity();
        subject7.setName("Cпец.флейта");
        subject7.setWorkTypeEntity(workInd);
        SubjectEntity subject8 = new SubjectEntity();
        subject8.setName("Cпец.труба");
        subject8.setWorkTypeEntity(workInd);
        SubjectEntity subject9 = new SubjectEntity();
        subject9.setName("Cпец.саксофон");
        subject9.setWorkTypeEntity(workInd);
        SubjectEntity subject10 = new SubjectEntity();
        subject10.setName("Cпец.гобой");
        subject10.setWorkTypeEntity(workInd);
        SubjectEntity subject11 = new SubjectEntity();
        subject11.setName("Cпец.тромбон");
        subject11.setWorkTypeEntity(workInd);
        SubjectEntity subject12 = new SubjectEntity();
        subject12.setName("Cпец.скрипка");
        subject12.setWorkTypeEntity(workInd);
        SubjectEntity subject13 = new SubjectEntity();
        subject13.setName("Cпец.виолончель");
        subject13.setWorkTypeEntity(workInd);
        SubjectEntity subject14 = new SubjectEntity();
        subject14.setName("Cпец.ударные");
        subject14.setWorkTypeEntity(workInd);
        SubjectEntity subject15 = new SubjectEntity();
        subject15.setName("Cпец.вокал академ");
        subject15.setWorkTypeEntity(workInd);
        SubjectEntity subject16 = new SubjectEntity();
        subject16.setName("Cпец.вокал эстрада");
        subject16.setWorkTypeEntity(workInd);
        SubjectEntity subject17 = new SubjectEntity();
        subject17.setName("Cпец. синтезатор");
        subject17.setWorkTypeEntity(workInd);
        SubjectEntity subject18 = new SubjectEntity();
        subject18.setName("Cпец.баян");
        subject18.setWorkTypeEntity(workInd);
        SubjectEntity subject19 = new SubjectEntity();
        subject19.setName("Cпец.аккордион");
        subject19.setWorkTypeEntity(workInd);
        SubjectEntity subject20 = new SubjectEntity();
        subject20.setName("Cпец.гитара");
        subject20.setWorkTypeEntity(workInd);
        SubjectEntity subject21 = new SubjectEntity();
        subject21.setName("Cпец.бандура");
        subject21.setWorkTypeEntity(workInd);
        subjectEntities.add(subject);
        subjectEntities.add(subject1);
        subjectEntities.add(subject2);
        subjectEntities.add(subject3);
        subjectEntities.add(subject4);
        subjectEntities.add(subject5);
        subjectEntities.add(subject6);
        subjectEntities.add(subject7);
        subjectEntities.add(subject8);
        subjectEntities.add(subject9);
        subjectEntities.add(subject10);
        subjectEntities.add(subject11);
        subjectEntities.add(subject12);
        subjectEntities.add(subject13);
        subjectEntities.add(subject14);
        subjectEntities.add(subject16);
        subjectEntities.add(subject17);
        subjectEntities.add(subject18);
        subjectEntities.add(subject19);
        subjectEntities.add(subject20);
        subjectEntities.add(subject21);
        List<LessonEntity> lessonEntities = new ArrayList<>();
        int startInt = 12;
        for (int i = 1; i < 8; i++) {
            String startTime = startInt+":00";
            String endTime = startInt+":50";
            LessonEntity lessonEntity = new LessonEntity();
            lessonEntity.setLessonNumber(i);
            lessonEntity.setStartTime(startTime);
            lessonEntity.setEndTime(endTime);
            lessonEntities.add(lessonEntity);
            startInt++;
        }
        applicationContext.getBean(TestService.class).cleanAll();
        applicationContext.getBean(TestService.class).fillStaticTables(yearEntities, officeEntities,
                                                                       workTypeEntities, subjectEntities,
                                                                       lessonEntities);
        applicationContext.getBean(TestService.class).addAdmin();
    }
}
