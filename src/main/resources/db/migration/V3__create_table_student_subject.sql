CREATE TABLE IF NOT EXISTS "student_subject"
(
    id         SERIAL PRIMARY KEY,
    student_id      BIGINT      NOT NULL,
	subject_id      BIGINT      NOT NULL,
	teacher_subject_id      BIGINT      NOT NULL,
	constraint "student_id_fkey" FOREIGN KEY (student_id) REFERENCES "student" (id),
	constraint "subject_id_fkey" FOREIGN KEY (subject_id) REFERENCES "subject" (id),
	constraint "teacher_subject_id_fkey" FOREIGN KEY (teacher_subject_id) REFERENCES "teacher_subject" (id)
);

DROP TABLE "schedule";

CREATE TABLE IF NOT EXISTS "schedule"
(
    id         SERIAL PRIMARY KEY,
    start_date DATE NOT NULL,
	end_date DATE NOT NULL,
	week_day VARCHAR(20) NOT NULL,
    teacher_subject_id      BIGINT      NOT NULL,
	teacher_id     			BIGINT      NOT NULL,
	teacher_student_id      BIGINT      NOT NULL,
	group_composition_id      BIGINT      NOT NULL,
	community_composition_id      BIGINT      NOT NULL,
	student_subject_id  BIGINT  NOT NULL,
	lesson_id      BIGINT      NOT NULL,
	constraint "lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES "lesson" (id),
	constraint "teacher_subject_id_fkey" FOREIGN KEY (teacher_subject_id) REFERENCES "teacher_subject" (id),
	constraint "group_composition_id_fkey" FOREIGN KEY (group_composition_id) REFERENCES "group_composition" (id),
	constraint "community_composition_id_fkey" FOREIGN KEY (community_composition_id) REFERENCES "community_composition" (id),
	constraint "teacher_id_fkey" FOREIGN KEY (teacher_id) 		 REFERENCES "teacher" (id),
	constraint "teacher_student_id_fkey" FOREIGN KEY (teacher_student_id) REFERENCES "teacher_student" (id),
	constraint "student_subject_id_fkey" FOREIGN KEY (student_subject_id) REFERENCES "student_subject" (id)
);