DROP TABLE "schedule";

CREATE TABLE IF NOT EXISTS "schedule"
(
    id         SERIAL PRIMARY KEY,
    start_date DATE NOT NULL,
	end_date DATE NOT NULL,
	week_day VARCHAR(20) NOT NULL,
    teacher_subject_id      BIGINT      NOT NULL,
	teacher_id     			BIGINT      NOT NULL,
	teacher_student_id      BIGINT,
	group_id      BIGINT,
	community_id      BIGINT,
	student_subject_id  BIGINT,
	lesson_id      BIGINT      NOT NULL,
	constraint "lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES "lesson" (id),
	constraint "teacher_subject_id_fkey" FOREIGN KEY (teacher_subject_id) REFERENCES "teacher_subject" (id),
	constraint "group_id_fkey" FOREIGN KEY (group_id) REFERENCES "student_group" (id),
	constraint "community_id_fkey" FOREIGN KEY (community_id) REFERENCES "community" (id),
	constraint "teacher_id_fkey" FOREIGN KEY (teacher_id) 		 REFERENCES "teacher" (id),
	constraint "teacher_student_id_fkey" FOREIGN KEY (teacher_student_id) REFERENCES "teacher_student" (id),
	constraint "student_subject_id_fkey" FOREIGN KEY (student_subject_id) REFERENCES "student_subject" (id)
);