CREATE TABLE IF NOT EXISTS "education_year"
(
    id           SERIAL PRIMARY KEY,
    year_number  INT      NOT NULL
);

CREATE TABLE IF NOT EXISTS "office"
(
    id           SERIAL PRIMARY KEY,
    office_number  INT      NOT NULL
);

CREATE TABLE IF NOT EXISTS "work_type"
(
    id           SERIAL PRIMARY KEY,
    "name"  VARCHAR(50)      NOT NULL
);

CREATE TABLE IF NOT EXISTS "student"
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
	second_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    education_year_id      BIGINT      NOT NULL,
	constraint "education_year_id_fkey" FOREIGN KEY (education_year_id) REFERENCES "education_year" (id)
);

CREATE TABLE IF NOT EXISTS "teacher"
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
	second_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    office_id      BIGINT      NOT NULL,
	constraint "office_id_fkey" FOREIGN KEY (office_id) REFERENCES "office" (id)
);

CREATE TABLE IF NOT EXISTS "subject"
(
    id         SERIAL PRIMARY KEY,
    "name" VARCHAR(50) NOT NULL,
    work_type_id      BIGINT      NOT NULL,
	constraint "work_type_fkey" FOREIGN KEY (work_type_id) REFERENCES "work_type" (id)
);

CREATE TABLE IF NOT EXISTS "lesson"
(
    id         SERIAL PRIMARY KEY,
    lesson_number INT NOT NULL,
	start_time VARCHAR(5) NOT NULL,
    end_time  VARCHAR(5) NOT NULL
);

CREATE TABLE IF NOT EXISTS "teacher_student"
(
    id         SERIAL PRIMARY KEY,
    teacher_id      BIGINT      NOT NULL,
	student_id      BIGINT      NOT NULL,
	constraint "teacher_id_fkey" FOREIGN KEY (teacher_id) REFERENCES "teacher" (id),
	constraint "student_id_fkey" FOREIGN KEY (student_id) REFERENCES "student" (id)
);

CREATE TABLE IF NOT EXISTS "community"
(
    id           SERIAL PRIMARY KEY,
    "name"  VARCHAR(50)      NOT NULL,
	teacher_id BIGINT NOT NULL,
	constraint "teacher_fkey" FOREIGN KEY (teacher_id) REFERENCES "teacher" (id)
);

CREATE TABLE IF NOT EXISTS "community_composition"
(
    id         SERIAL PRIMARY KEY,
    community_id      BIGINT      NOT NULL,
	student_id      BIGINT      NOT NULL,
	constraint "community_id_fkey" FOREIGN KEY (community_id) REFERENCES "community" (id),
	constraint "student_id_fkey" FOREIGN KEY (student_id) REFERENCES "student" (id)
);

CREATE TABLE IF NOT EXISTS "student_group"
(
    id           SERIAL PRIMARY KEY,
	education_year_id BIGINT NOT NULL,
	constraint "education_year_fkey" FOREIGN KEY (education_year_id) REFERENCES "education_year" (id)
);

CREATE TABLE IF NOT EXISTS "group_composition"
(
    id         SERIAL PRIMARY KEY,
    group_id      BIGINT      NOT NULL,
	student_id      BIGINT      NOT NULL,
	constraint "student_group_id_fkey" FOREIGN KEY (group_id) REFERENCES "student_group" (id),
	constraint "student_id_fkey" FOREIGN KEY (student_id) REFERENCES "student" (id)
);

CREATE TABLE IF NOT EXISTS "teacher_subject"
(
    id         SERIAL PRIMARY KEY,
    teacher_id      BIGINT      NOT NULL,
	subject_id      BIGINT      NOT NULL,
	constraint "teacher_id_fkey" FOREIGN KEY (teacher_id) REFERENCES "teacher" (id),
	constraint "subject_id_fkey" FOREIGN KEY (subject_id) REFERENCES "subject" (id)
);

CREATE TABLE IF NOT EXISTS "schedule"
(
    id         SERIAL PRIMARY KEY,
    start_date DATE NOT NULL,
	end_date DATE NOT NULL,
	week_day VARCHAR(20) NOT NULL,
    teacher_subject_id      BIGINT      NOT NULL,
	teacher_id     			BIGINT      NOT NULL,
	teacher_student_id      BIGINT      NOT NULL,
	group_composition_id      BIGINT      NOT NULL,
	community_composition_id      BIGINT      NOT NULL,
	lesson_id      BIGINT      NOT NULL,
	constraint "lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES "lesson" (id),
	constraint "teacher_subject_id_fkey" FOREIGN KEY (teacher_subject_id) REFERENCES "teacher_subject" (id),
	constraint "group_composition_id_fkey" FOREIGN KEY (group_composition_id) REFERENCES "group_composition" (id),
	constraint "community_composition_id_fkey" FOREIGN KEY (community_composition_id) REFERENCES "community_composition" (id),
	constraint "teacher_id_fkey" FOREIGN KEY (teacher_id) 		 REFERENCES "teacher" (id),
	constraint "teacher_student_id_fkey" FOREIGN KEY (teacher_student_id) REFERENCES "teacher_student" (id)
);